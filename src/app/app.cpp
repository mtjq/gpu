#include "app.h"

#include "core/input.h"
#include "render/gpu.h"
#include "render/renderer.h"


const unsigned int c_width = 1920;
const unsigned int c_height = 1080;


static void framebufferResizeCallback(GLFWwindow* _window,
                                      int _width, int _height) {
  // TODO: synchronise app callback with real renderer swapchain resize.
  App* app = reinterpret_cast<App*>(glfwGetWindowUserPointer(_window));
  app->setSwapchainSizeDirty(true);
  app->onFrameBufferResize(_width, _height);
}


App::App() {
  init();
}

App::~App() {
  close();
}

void App::init() {
  std::srand(std::time(nullptr));

  initWindow();

  Input::init(m_window);

  Gpu::init(m_window);
  Renderer::init(m_window);

  const VkExtent2D extent = Renderer::getSwapchainExtent();
  m_camera.setAspectRatio(extent.width / (float) extent.height);

  m_boids.init();
}

void App::initWindow() {
  glfwInit();

  glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
  glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

  m_window = glfwCreateWindow(c_width, c_height, "GPU adventures",
                              nullptr, nullptr);
  glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
  glfwSetWindowUserPointer(m_window, this);
  glfwSetFramebufferSizeCallback(m_window, framebufferResizeCallback);
}

void App::onFrameBufferResize(int _width, int _height) {
  m_camera.setAspectRatio(_width / (float) _height);
}

void App::setSwapchainSizeDirty(bool _val) {
  Renderer::setFramebufferSizeDirty(_val);
}

void App::close() {
  m_boids.close();

  Renderer::cleanup();
  Gpu::cleanup();

  glfwDestroyWindow(m_window);
  glfwTerminate();
}

void App::run() {
  m_previous_frame_time = glfwGetTime();
  while (!glfwWindowShouldClose(m_window)) {
    float current_time = glfwGetTime();
    float delta_time = current_time - m_previous_frame_time;
    glfwPollEvents();
    update(delta_time);
    Input::endFrame();
    m_previous_frame_time = current_time;
  }

  Gpu::deviceWait();
}

// void static toggleCursorInputMode(GLFWwindow* _window) {
//   if (glfwGetInputMode(_window, GLFW_CURSOR) != GLFW_CURSOR_NORMAL) {
//     glfwSetInputMode(_window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
//   } else {
//     glfwSetInputMode(_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
//   }
// }

void App::update(float _delta_time) {
  processInputs(_delta_time);

  VkCommandBuffer& command_buffer =
      Renderer::beginFrame(m_camera.getViewProj());

  m_boids.compute(command_buffer, _delta_time);
  Renderer::beginRenderPass(command_buffer);
  m_boids.renderImGui(command_buffer);
  m_boids.render(command_buffer, m_camera);
  Renderer::endRenderPass(command_buffer);

  Renderer::endFrame(command_buffer);
}


void App::processInputs(float _delta_time) {
  // if (Input::isKeyJustPressed(GLFW_KEY_SPACE)) {
  //   toggleCursorInputMode(m_window);
  // }

  glm::vec3 input{0.0f};
  if (Input::isKeyPressed(GLFW_KEY_W)) {
    input += glm::vec3{0.0f, 1.0f, 0.0f};
  }
  if (Input::isKeyPressed(GLFW_KEY_S)) {
    input += glm::vec3{0.0f, -1.0f, 0.0f};
  }
  if (Input::isKeyPressed(GLFW_KEY_A)) {
    input += glm::vec3{-1.0f, 0.0f, 0.0f};
  }
  if (Input::isKeyPressed(GLFW_KEY_D)) {
    input += glm::vec3{1.0f, 0.0f, 0.0f};
  }
  if (Input::isKeyPressed(GLFW_KEY_Q)) {
    input += glm::vec3{0.0f, 0.0f, -1.0f};
  }
  if (Input::isKeyPressed(GLFW_KEY_E)) {
    input += glm::vec3{0.0f, 0.0f, 1.0f};
  }
  if (Input::isKeyPressed(GLFW_KEY_LEFT_SHIFT)
      || Input::isKeyPressed(GLFW_KEY_RIGHT_SHIFT)) {
    input *= 4.0;
  }
  if (Input::isKeyPressed(GLFW_KEY_LEFT_CONTROL)
      || Input::isKeyPressed(GLFW_KEY_RIGHT_CONTROL)) {
    input *= 0.25;
  }
  m_camera.translate(input, _delta_time);

  // if (glfwGetInputMode(m_window, GLFW_CURSOR) == GLFW_CURSOR_DISABLED) {
  if (Input::isMouseButtonPressed(GLFW_MOUSE_BUTTON_RIGHT)) {
    m_camera.rotate(Input::getCursorMovement(), _delta_time);
  }

  // if(Input::isMouseButtonJustPressed(GLFW_MOUSE_BUTTON_RIGHT)) {
  //   glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
  // } else if (Input::isMouseButtonJustReleased(GLFW_MOUSE_BUTTON_RIGHT)) {
  //   glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
  // }

  m_camera.updateView();
}
