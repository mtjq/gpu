#pragma once

#include "render/camera.h"
#include "render/renderer.h"


struct BoidsComputeUniformObject {
  alignas(16) glm::vec3    m_box_size{80.0f, 40.0f, 40.0f};
  alignas(4)  float        m_turn_acceleration{100.0f};
  alignas(4)  float        m_min_speed{25.0f};
  alignas(4)  float        m_max_speed{35.0f};
  alignas(4)  float        m_separation_range{2.0f};
  alignas(4)  float        m_separation_factor{4.0f};
  alignas(4)  float        m_alignment_range{4.0f};
  alignas(4)  float        m_alignment_factor{2.0f};
  alignas(4)  float        m_cohesion_range{5.0f};
  alignas(4)  float        m_cohesion_factor{4.0f};
  alignas(4)  unsigned int m_boid_count{512};
  alignas(4)  float        m_delta_time{0.0f};
};

class Boids {
public:
  Boids() = default;
  ~Boids() = default;

  void init();
  void compute(VkCommandBuffer& _command_buffer, float _delta_time);
  void render(VkCommandBuffer& _command_buffer, Camera& _camera);
  void renderImGui(VkCommandBuffer& _command_buffer);
  void close();

private:
  float m_boid_size{1.0f};

  std::array<VkBuffer, c_frames_in_flight> m_boids_buffer{VK_NULL_HANDLE};
  std::array<VkDeviceMemory, c_frames_in_flight>
                                          m_boids_buffer_memory{VK_NULL_HANDLE};


  BoidsComputeUniformObject m_compute_uniform_object{};
  std::array<VkBuffer, c_frames_in_flight>
                            m_compute_uniform_buffers{VK_NULL_HANDLE};
  std::array<VkDeviceMemory, c_frames_in_flight>
                            m_compute_uniform_buffers_memories{VK_NULL_HANDLE};
  std::array<void*, c_frames_in_flight>
                            m_compute_uniform_buffers_mapped{nullptr};

  VkPipeline            m_compute_pipeline{VK_NULL_HANDLE};
  VkDescriptorPool      m_compute_descriptor_pool{VK_NULL_HANDLE};
  VkDescriptorSetLayout m_compute_descriptor_set_layout{VK_NULL_HANDLE};
  std::array<VkDescriptorSet, c_frames_in_flight>
                        m_compute_descriptor_sets{VK_NULL_HANDLE};
  VkPipelineLayout      m_compute_pipeline_layout{VK_NULL_HANDLE};


  std::array<VkBuffer, c_frames_in_flight>
      m_render_uniform_buffers{VK_NULL_HANDLE};
  std::array<VkDeviceMemory, c_frames_in_flight>
      m_render_uniform_buffers_memories{VK_NULL_HANDLE};
  std::array<void*, c_frames_in_flight>
      m_render_uniform_buffers_mapped{nullptr};

  VkPipeline            m_render_pipeline{VK_NULL_HANDLE};
  VkDescriptorPool      m_render_descriptor_pool{VK_NULL_HANDLE};
  VkDescriptorSetLayout m_render_descriptor_set_layout{VK_NULL_HANDLE};
  std::array<VkDescriptorSet, c_frames_in_flight>
                        m_render_descriptor_sets{VK_NULL_HANDLE};
  VkPipelineLayout      m_render_pipeline_layout{VK_NULL_HANDLE};

  void createComputePipeline();
  void createComputeDescriptorPool();
  void createComputeDescriptorSetLayout();
  void createComputeDescriptorSets();
  void createComputeUniformBuffer();

  void createRenderPipeline();
  void createRenderDescriptorPool();
  void createRenderDescriptorSetLayout();
  void createRenderDescriptorSets();
  void createRenderUniformBuffer();

  void createBoidsBuffer();

  void updateComputeUniformBuffer(float _delta_time);
};

