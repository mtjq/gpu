#pragma once

#include "boids.h"
#include "render/camera.h"


class App {
public:
  App();
  App(const App&) = delete;
  App(App&&) = delete;
  App& operator=(const App&) = delete;
  App& operator=(App&&) = delete;
  ~App();

  void run();
  void onFrameBufferResize(int _width, int _height);
  void setSwapchainSizeDirty(bool _val);

private:
  GLFWwindow* m_window{nullptr};
  float       m_previous_frame_time{0.0f};
  Camera      m_camera{};
  Boids       m_boids{};

  void init();
  void initWindow();
  void update(float _delta_time);
  void processInputs(float _delta_time);
  void close();
};

