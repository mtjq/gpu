#include "boids.h"

#include "imgui/imgui.h"

#include "core/filesystem_manager.h"
#include "core/utils.h"
#include "render/gpu.h"
#include "render/renderer.h"


struct Boid {
  using AttributesDesc =
      std::array<VkVertexInputAttributeDescription, 2>;

  alignas(16) glm::vec3 m_position;
  alignas(16) glm::vec3 m_velocity;

  static VkVertexInputBindingDescription getBindingDescription() {
    VkVertexInputBindingDescription binding_description{};
    binding_description.binding = 0;
    binding_description.stride = sizeof(Boid);
    binding_description.inputRate = VK_VERTEX_INPUT_RATE_INSTANCE;

    return binding_description;
  }

  static AttributesDesc getAttributeDescriptions() {
    AttributesDesc attribute_descriptions{};

    attribute_descriptions[0].binding = 0;
    attribute_descriptions[0].location = 0;
    attribute_descriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
    attribute_descriptions[0].offset = offsetof(Boid, m_position);

    attribute_descriptions[1].binding = 0;
    attribute_descriptions[1].location = 1;
    attribute_descriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
    attribute_descriptions[1].offset = offsetof(Boid, m_velocity);

    return attribute_descriptions;
  }
};

struct BoidsRenderUniformObject {
  alignas(64) glm::mat4 m_view_proj;
  alignas(4)  float     m_size;
};

void Boids::init() {
  createBoidsBuffer();
  createRenderUniformBuffer();
  createComputeUniformBuffer();
  createComputeDescriptorPool();
  createComputeDescriptorSetLayout();
  createComputeDescriptorSets();
  createComputePipeline();
  createRenderDescriptorPool();
  createRenderDescriptorSetLayout();
  createRenderDescriptorSets();
  createRenderPipeline();

  Renderer::initBoxRendering();
}

void Boids::compute(VkCommandBuffer& _command_buffer, float _delta_time) {
  updateComputeUniformBuffer(_delta_time);
  unsigned int frame = Renderer::getCurrentFrameInFlight();
  vkCmdBindPipeline(_command_buffer, VK_PIPELINE_BIND_POINT_COMPUTE,
                    m_compute_pipeline);
  vkCmdBindDescriptorSets(_command_buffer, VK_PIPELINE_BIND_POINT_COMPUTE,
                          m_compute_pipeline_layout, 0, 1,
                          &m_compute_descriptor_sets[frame], 0, 0);

  vkCmdDispatch(_command_buffer,
                m_compute_uniform_object.m_boid_count / 64, 1, 1);
}

void Boids::render(VkCommandBuffer& _command_buffer, Camera& _camera) {
  unsigned int frame = Renderer::getCurrentFrameInFlight();

  BoidsRenderUniformObject ubo{_camera.getViewProj(), m_boid_size};
  memcpy(m_render_uniform_buffers_mapped[frame], &ubo,
         sizeof(BoidsRenderUniformObject));

  vkCmdBindPipeline(_command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
                    m_render_pipeline);
  VkBuffer vertex_buffers[] = {m_boids_buffer[frame]};
  VkDeviceSize offsets[] = {0};
  vkCmdBindVertexBuffers(_command_buffer, 0, 1, vertex_buffers, offsets);
  vkCmdBindDescriptorSets(_command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
                          m_render_pipeline_layout, 0, 1,
                          &m_render_descriptor_sets[frame], 0, nullptr);
  vkCmdDraw(_command_buffer, 12, m_compute_uniform_object.m_boid_count, 0, 0);

  glm::mat4 model = glm::mat4(1.0f);
  model = glm::scale(model, m_compute_uniform_object.m_box_size);
  Renderer::addBox(ColouredInstance{model, glm::vec4{0.8f, 0.8f, 0.8f, 1.0f}},
                   true);
  Renderer::addBox(ColouredInstance{model, glm::vec4{0.7f, 0.7f, 0.7f, 0.1f}},
                   false);

  Renderer::renderBoxes(_command_buffer);
}

void Boids::close() {
    VkDevice device = Gpu::getDevice();

  for (unsigned int i = 0; i < c_frames_in_flight; ++i) {
    vkFreeMemory(device, m_boids_buffer_memory[i], nullptr);
    vkDestroyBuffer(device, m_boids_buffer[i], nullptr);
    vkFreeMemory(device, m_render_uniform_buffers_memories[i], nullptr);
    vkDestroyBuffer(device, m_render_uniform_buffers[i], nullptr);
    vkFreeMemory(device, m_compute_uniform_buffers_memories[i], nullptr);
    vkDestroyBuffer(device, m_compute_uniform_buffers[i], nullptr);
  }

  vkDestroyPipeline(device, m_compute_pipeline, nullptr);
  vkDestroyPipelineLayout(device, m_compute_pipeline_layout, nullptr);
  vkDestroyDescriptorSetLayout(device, m_compute_descriptor_set_layout,
                               nullptr);
  vkDestroyDescriptorPool(device, m_compute_descriptor_pool, nullptr);

  vkDestroyPipeline(device, m_render_pipeline, nullptr);
  vkDestroyPipelineLayout(device, m_render_pipeline_layout, nullptr);
  vkDestroyDescriptorSetLayout(device, m_render_descriptor_set_layout, nullptr);
  vkDestroyDescriptorPool(device, m_render_descriptor_pool, nullptr);
}

void Boids::createComputePipeline() {
  VkDevice device = Gpu::getDevice();

  std::vector<char> comp_shader_code = readFile(
      FilesystemManager::FromRootDirRelativeToAbsolute(
          "assets/shaders/boids_comp.spv"));

  VkShaderModule comp_shader_module =
      Renderer::createShaderModule(comp_shader_code);

  VkPipelineShaderStageCreateInfo shader_stage_info{};
  shader_stage_info.sType =
      VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
  shader_stage_info.stage = VK_SHADER_STAGE_COMPUTE_BIT;
  shader_stage_info.module = comp_shader_module;
  shader_stage_info.pName = "main";

  VkPipelineLayoutCreateInfo pipeline_layout_info{};
  pipeline_layout_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
  pipeline_layout_info.setLayoutCount = 1;
  pipeline_layout_info.pSetLayouts = &m_compute_descriptor_set_layout;

  VkResult create_layout_result =
      vkCreatePipelineLayout(device, &pipeline_layout_info, nullptr,
                             &m_compute_pipeline_layout);
  if (create_layout_result != VK_SUCCESS) {
    throw std::runtime_error("failed to create boids compute pipeline layout");
  }

  VkComputePipelineCreateInfo pipeline_info{};
  pipeline_info.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
  pipeline_info.layout = m_compute_pipeline_layout;
  pipeline_info.stage = shader_stage_info;

  VkResult create_pipeline_result =
      vkCreateComputePipelines(device, VK_NULL_HANDLE, 1, &pipeline_info,
                               nullptr, &m_compute_pipeline);
  if (create_pipeline_result != VK_SUCCESS) {
    throw std::runtime_error("failed to create boids compute pipeline");
  }

  vkDestroyShaderModule(device, comp_shader_module, nullptr);
}

void Boids::createComputeDescriptorPool() {
  std::array<VkDescriptorPoolSize, 2> pool_sizes;
  pool_sizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
  pool_sizes[0].descriptorCount = c_frames_in_flight;
  pool_sizes[1].type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
  pool_sizes[1].descriptorCount = 2 * c_frames_in_flight;

  VkDescriptorPoolCreateInfo pool_info{};
  pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
  pool_info.poolSizeCount = pool_sizes.size();
  pool_info.pPoolSizes = pool_sizes.data();
  pool_info.maxSets = 3 * c_frames_in_flight;

  VkResult create_result =
      vkCreateDescriptorPool(Gpu::getDevice(), &pool_info, nullptr,
                             &m_compute_descriptor_pool);
  if (create_result != VK_SUCCESS) {
    throw std::runtime_error("failed to create boids compute descriptor pool");
  }
}

void Boids::createComputeDescriptorSetLayout() {
  std::array<VkDescriptorSetLayoutBinding, 3> layout_bindings;
  layout_bindings[0].binding = 0;
  layout_bindings[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
  layout_bindings[0].descriptorCount = 1;
  layout_bindings[0].stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;

  layout_bindings[1].binding = 1;
  layout_bindings[1].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
  layout_bindings[1].descriptorCount = 1;
  layout_bindings[1].stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;

  layout_bindings[2].binding = 2;
  layout_bindings[2].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
  layout_bindings[2].descriptorCount = 1;
  layout_bindings[2].stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;

  VkDescriptorSetLayoutCreateInfo layout_info{};
  layout_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
  layout_info.bindingCount = layout_bindings.size();
  layout_info.pBindings = layout_bindings.data();

  VkResult create_result =
      vkCreateDescriptorSetLayout(Gpu::getDevice(), &layout_info, nullptr,
                                  &m_compute_descriptor_set_layout);
  if (create_result != VK_SUCCESS) {
    throw std::runtime_error("failed to create boids "
                             "compute descriptor set layout");
  }
}

void Boids::createComputeDescriptorSets() {
  VkDevice& device = Gpu::getDevice();

  std::array<VkDescriptorSetLayout, c_frames_in_flight> layouts;
  layouts.fill(m_compute_descriptor_set_layout);
  VkDescriptorSetAllocateInfo alloc_info{};
  alloc_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
  alloc_info.descriptorPool = m_compute_descriptor_pool;
  alloc_info.descriptorSetCount = layouts.size();
  alloc_info.pSetLayouts = layouts.data();

  VkResult allocate_result =
      vkAllocateDescriptorSets(device, &alloc_info,
                               m_compute_descriptor_sets.data());
  if (allocate_result != VK_SUCCESS) {
    throw std::runtime_error("failed to allocate boids compute descriptor sets");
  }

  for (size_t i = 0; i < c_frames_in_flight; ++i) {
    std::array<VkWriteDescriptorSet, 3> descriptor_writes;

    VkDescriptorBufferInfo uniform_buffer_info{};
    uniform_buffer_info.buffer = m_compute_uniform_buffers[i];
    uniform_buffer_info.offset = 0;
    uniform_buffer_info.range = sizeof(BoidsComputeUniformObject);

    descriptor_writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptor_writes[0].dstSet = m_compute_descriptor_sets[i];
    descriptor_writes[0].dstBinding = 0;
    descriptor_writes[0].dstArrayElement = 0;
    descriptor_writes[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptor_writes[0].descriptorCount = 1;
    descriptor_writes[0].pBufferInfo = &uniform_buffer_info;
    descriptor_writes[0].pNext = nullptr;

    VkDescriptorBufferInfo storage_buffer_info_previous_frame{};
    storage_buffer_info_previous_frame.buffer =
        m_boids_buffer[(i + c_frames_in_flight - 1) % c_frames_in_flight];
    storage_buffer_info_previous_frame.offset = 0;
    storage_buffer_info_previous_frame.range =
        m_compute_uniform_object.m_boid_count * sizeof(Boid);

    descriptor_writes[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptor_writes[1].dstSet = m_compute_descriptor_sets[i];
    descriptor_writes[1].dstBinding = 1;
    descriptor_writes[1].dstArrayElement = 0;
    descriptor_writes[1].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    descriptor_writes[1].descriptorCount = 1;
    descriptor_writes[1].pBufferInfo = &storage_buffer_info_previous_frame;
    descriptor_writes[1].pNext = nullptr;

    VkDescriptorBufferInfo storage_buffer_info_current_frame{};
    storage_buffer_info_current_frame.buffer = m_boids_buffer[i];
    storage_buffer_info_current_frame.offset = 0;
    storage_buffer_info_current_frame.range =
        m_compute_uniform_object.m_boid_count * sizeof(Boid);

    descriptor_writes[2].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptor_writes[2].dstSet = m_compute_descriptor_sets[i];
    descriptor_writes[2].dstBinding = 2;
    descriptor_writes[2].dstArrayElement = 0;
    descriptor_writes[2].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    descriptor_writes[2].descriptorCount = 1;
    descriptor_writes[2].pBufferInfo = &storage_buffer_info_current_frame;
    descriptor_writes[2].pNext = nullptr;

    vkUpdateDescriptorSets(device, descriptor_writes.size(),
                           descriptor_writes.data(), 0, nullptr);
  }
}

void Boids::createComputeUniformBuffer() {
  VkDeviceSize buffer_size = sizeof(BoidsComputeUniformObject);

  for (size_t i = 0; i < c_frames_in_flight; ++i) {
    VkMemoryPropertyFlags memory_properties =
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
        VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
    Renderer::createBuffer(buffer_size, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                           memory_properties,
                           m_compute_uniform_buffers[i],
                           m_compute_uniform_buffers_memories[i]);

  vkMapMemory(Gpu::getDevice(), m_compute_uniform_buffers_memories[i],
              0, buffer_size, 0, &m_compute_uniform_buffers_mapped[i]);
  }
}

void Boids::createRenderPipeline() {
  VkDevice device = Gpu::getDevice();

  std::vector<char> vert_shader_code = readFile(
      FilesystemManager::FromRootDirRelativeToAbsolute(
          "assets/shaders/boids_vert.spv"));
  std::vector<char> frag_shader_code = readFile(
      FilesystemManager::FromRootDirRelativeToAbsolute(
          "assets/shaders/boids_frag.spv"));

  VkShaderModule vert_shader_module =
      Renderer::createShaderModule(vert_shader_code);
  VkShaderModule frag_shader_module =
      Renderer::createShaderModule(frag_shader_code);

  VkPipelineShaderStageCreateInfo vert_shader_stage_info{};
  vert_shader_stage_info.sType =
      VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
  vert_shader_stage_info.stage = VK_SHADER_STAGE_VERTEX_BIT;
  vert_shader_stage_info.module = vert_shader_module;
  vert_shader_stage_info.pName = "main";

  VkPipelineShaderStageCreateInfo frag_shader_stage_info{};
  frag_shader_stage_info.sType =
      VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
  frag_shader_stage_info.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
  frag_shader_stage_info.module = frag_shader_module;
  frag_shader_stage_info.pName = "main";

  VkPipelineShaderStageCreateInfo shader_stages[] = {vert_shader_stage_info,
                                                     frag_shader_stage_info};

  VkVertexInputBindingDescription binding_description =
      Boid::getBindingDescription();
  auto attribute_descriptions = Boid::getAttributeDescriptions();
  VkPipelineVertexInputStateCreateInfo vertex_input_info{};
  vertex_input_info.sType =
      VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
  vertex_input_info.vertexBindingDescriptionCount = 1;
  vertex_input_info.pVertexBindingDescriptions = &binding_description;
  vertex_input_info.vertexAttributeDescriptionCount =
      attribute_descriptions.size();
  vertex_input_info.pVertexAttributeDescriptions =
      attribute_descriptions.data();

  VkPipelineInputAssemblyStateCreateInfo input_assembly{};
  input_assembly.sType =
      VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
  input_assembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
  input_assembly.primitiveRestartEnable = VK_FALSE;

  VkExtent2D swapchain_extent = Renderer::getSwapchainExtent();

  VkViewport viewport{};
  viewport.x = 0.0f;
  viewport.y = 0.0f;
  viewport.width = (float) swapchain_extent.width;
  viewport.height = (float) swapchain_extent.height;
  viewport.minDepth = 0.0f;
  viewport.maxDepth = 1.0f;

  VkRect2D scissor{};
  scissor.offset = {0, 0};
  scissor.extent = swapchain_extent;

  VkPipelineViewportStateCreateInfo viewport_state{};
  viewport_state.sType =
      VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
  viewport_state.viewportCount = 1;
  viewport_state.pViewports = &viewport;
  viewport_state.scissorCount = 1;
  viewport_state.pScissors = &scissor;

  std::array<VkDynamicState, 2> dynamic_states = {VK_DYNAMIC_STATE_VIEWPORT,
                                                  VK_DYNAMIC_STATE_SCISSOR};

  VkPipelineDynamicStateCreateInfo dynamic_state{};
  dynamic_state.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
  dynamic_state.dynamicStateCount = dynamic_states.size();
  dynamic_state.pDynamicStates = dynamic_states.data();

  VkPipelineRasterizationStateCreateInfo rasterizer{};
  rasterizer.sType =
      VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
  rasterizer.depthClampEnable = VK_FALSE;
  rasterizer.rasterizerDiscardEnable = VK_FALSE;
  rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
  rasterizer.lineWidth = 1.0f;
  rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
  rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
  rasterizer.depthBiasEnable = VK_FALSE;

  VkPipelineMultisampleStateCreateInfo multisampling{};
  multisampling.sType =
      VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
  multisampling.sampleShadingEnable = VK_TRUE;
  multisampling.minSampleShading = 0.2f;
  multisampling.rasterizationSamples = Gpu::getMsaaSamples();

  VkPipelineColorBlendAttachmentState colour_blend_attachment{};
  colour_blend_attachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT |
                                           VK_COLOR_COMPONENT_G_BIT |
                                           VK_COLOR_COMPONENT_B_BIT |
                                           VK_COLOR_COMPONENT_A_BIT;
  colour_blend_attachment.blendEnable = VK_FALSE;
  colour_blend_attachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
  colour_blend_attachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
  colour_blend_attachment.colorBlendOp = VK_BLEND_OP_ADD;
  colour_blend_attachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
  colour_blend_attachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
  colour_blend_attachment.alphaBlendOp = VK_BLEND_OP_ADD;

  VkPipelineColorBlendStateCreateInfo colour_blending{};
  colour_blending.sType =
      VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
  colour_blending.logicOpEnable = VK_FALSE;
  colour_blending.logicOp = VK_LOGIC_OP_COPY;
  colour_blending.attachmentCount = 1;
  colour_blending.pAttachments = &colour_blend_attachment;
  colour_blending.blendConstants[0] = 0.0f;
  colour_blending.blendConstants[1] = 0.0f;
  colour_blending.blendConstants[2] = 0.0f;
  colour_blending.blendConstants[3] = 0.0f;

  VkPipelineLayoutCreateInfo pipeline_layout_info{};
  pipeline_layout_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
  pipeline_layout_info.setLayoutCount = 1;
  pipeline_layout_info.pSetLayouts = &m_render_descriptor_set_layout;
  pipeline_layout_info.pushConstantRangeCount = 0;
  pipeline_layout_info.pPushConstantRanges = nullptr;

  VkResult create_layout_result =
      vkCreatePipelineLayout(device, &pipeline_layout_info, nullptr,
                             &m_render_pipeline_layout);
  if (create_layout_result != VK_SUCCESS) {
    throw std::runtime_error("failed to create boids render pipeline layout");
  }

  VkPipelineDepthStencilStateCreateInfo depth_stencil{};
  depth_stencil.sType =
      VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
  depth_stencil.depthTestEnable = VK_TRUE;
  depth_stencil.depthWriteEnable = VK_TRUE;
  depth_stencil.depthCompareOp = VK_COMPARE_OP_LESS;
  depth_stencil.depthBoundsTestEnable = VK_FALSE;
  depth_stencil.stencilTestEnable = VK_FALSE;

  VkGraphicsPipelineCreateInfo pipeline_info{};
  pipeline_info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
  pipeline_info.stageCount = 2;
  pipeline_info.pStages = shader_stages;
  pipeline_info.pVertexInputState = &vertex_input_info;
  pipeline_info.pInputAssemblyState = &input_assembly;
  pipeline_info.pViewportState = &viewport_state;
  pipeline_info.pRasterizationState = &rasterizer;
  pipeline_info.pMultisampleState = &multisampling;
  pipeline_info.pDepthStencilState = &depth_stencil;
  pipeline_info.pColorBlendState = &colour_blending;
  pipeline_info.pDynamicState = &dynamic_state;
  pipeline_info.layout = m_render_pipeline_layout;
  pipeline_info.renderPass = Renderer::getRenderPass();
  pipeline_info.subpass = 0;
  pipeline_info.basePipelineHandle = VK_NULL_HANDLE;
  pipeline_info.basePipelineIndex = -1;

  VkResult create_pipeline_result =
      vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, &pipeline_info,
                                nullptr, &m_render_pipeline);
  if (create_pipeline_result != VK_SUCCESS) {
    throw std::runtime_error("failed to create box render pipeline");
  }

  vkDestroyShaderModule(device, vert_shader_module, nullptr);
  vkDestroyShaderModule(device, frag_shader_module, nullptr);
}

void Boids::createRenderDescriptorPool() {
  VkDescriptorPoolSize pool_size;
  pool_size.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
  pool_size.descriptorCount = c_frames_in_flight;

  VkDescriptorPoolCreateInfo pool_info{};
  pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
  pool_info.poolSizeCount = 1;
  pool_info.pPoolSizes = &pool_size;
  pool_info.maxSets = c_frames_in_flight;

  VkResult create_result = vkCreateDescriptorPool(Gpu::getDevice(), &pool_info,
                                                  nullptr,
                                                  &m_render_descriptor_pool);
  if (create_result != VK_SUCCESS) {
    throw std::runtime_error("failed to create boids render descriptor pool");
  }
}

void Boids::createRenderDescriptorSetLayout() {
  VkDescriptorSetLayoutBinding ubo_layout_binding{};
  ubo_layout_binding.binding = 0;
  ubo_layout_binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
  ubo_layout_binding.descriptorCount = 1;
  ubo_layout_binding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
  ubo_layout_binding.pImmutableSamplers = nullptr;

  VkDescriptorSetLayoutCreateInfo layout_info{};
  layout_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
  layout_info.bindingCount = 1;
  layout_info.pBindings = &ubo_layout_binding;

  VkResult create_result =
      vkCreateDescriptorSetLayout(Gpu::getDevice(), &layout_info, nullptr,
                                  &m_render_descriptor_set_layout);
  if (create_result != VK_SUCCESS) {
    throw std::runtime_error("failed to create boids "
                             "render descriptor set layout");
  }
}

void Boids::createRenderDescriptorSets() {
  VkDevice& device = Gpu::getDevice();

  std::array<VkDescriptorSetLayout, c_frames_in_flight> layouts;
  layouts.fill(m_render_descriptor_set_layout);
  VkDescriptorSetAllocateInfo alloc_info{};
  alloc_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
  alloc_info.descriptorPool = m_render_descriptor_pool;
  alloc_info.descriptorSetCount = c_frames_in_flight;
  alloc_info.pSetLayouts = layouts.data();

  VkResult allocate_result =
      vkAllocateDescriptorSets(device, &alloc_info,
                               m_render_descriptor_sets.data());
  if (allocate_result != VK_SUCCESS) {
    throw std::runtime_error("failed to allocate boids render descriptor sets");
  }

  for (size_t i = 0; i < c_frames_in_flight; ++i) {
    VkDescriptorBufferInfo buffer_info{};
    buffer_info.buffer = m_render_uniform_buffers[i];
    buffer_info.offset = 0;
    buffer_info.range = sizeof(BoidsRenderUniformObject);

    VkWriteDescriptorSet descriptor_write;
    descriptor_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptor_write.dstSet = m_render_descriptor_sets[i];
    descriptor_write.dstBinding = 0;
    descriptor_write.dstArrayElement = 0;
    descriptor_write.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptor_write.descriptorCount = 1;
    descriptor_write.pBufferInfo = &buffer_info;
    descriptor_write.pNext = nullptr;

    vkUpdateDescriptorSets(device, 1, &descriptor_write, 0, nullptr);
  }
}

void Boids::createRenderUniformBuffer() {
  VkDeviceSize buffer_size = sizeof(BoidsRenderUniformObject);

  for (size_t i = 0; i < c_frames_in_flight; ++i) {
    VkMemoryPropertyFlags memory_properties =
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
        VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
    Renderer::createBuffer(buffer_size, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                           memory_properties,
                           m_render_uniform_buffers[i],
                           m_render_uniform_buffers_memories[i]);

  vkMapMemory(Gpu::getDevice(), m_render_uniform_buffers_memories[i],
              0, buffer_size, 0, &m_render_uniform_buffers_mapped[i]);
  }
}

void Boids::createBoidsBuffer() {
  VkDevice& device = Gpu::getDevice();

  VkDeviceSize buffer_size = m_compute_uniform_object.m_boid_count *
                             sizeof(Boid);

  std::vector<Boid> boids(m_compute_uniform_object.m_boid_count);

  for (Boid& boid : boids) {
    boid.m_position = m_compute_uniform_object.m_box_size * glm::vec3{
        static_cast<float>(std::rand()) / RAND_MAX - 0.5f,
        static_cast<float>(std::rand()) / RAND_MAX - 0.5f,
        static_cast<float>(std::rand()) / RAND_MAX - 0.5f};
    boid.m_velocity = m_compute_uniform_object.m_max_speed * glm::normalize(
        glm::vec3{static_cast<float>(std::rand()) / RAND_MAX - 0.5f,
                  static_cast<float>(std::rand()) / RAND_MAX - 0.5f,
                  static_cast<float>(std::rand()) / RAND_MAX - 0.5f});
  }

  VkBuffer staging_buffer;
  VkDeviceMemory staging_buffer_memory;

  VkMemoryPropertyFlags staging_memory_properties =
      VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
      VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
  Renderer::createBuffer(buffer_size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                         staging_memory_properties,
                         staging_buffer, staging_buffer_memory);

  void* data;
  vkMapMemory(device, staging_buffer_memory, 0, buffer_size, 0, &data);
  memcpy(data, boids.data(), static_cast<size_t>(buffer_size));
  vkUnmapMemory(device, staging_buffer_memory);

  VkBufferUsageFlags buffer_usage_flags =
      VK_BUFFER_USAGE_TRANSFER_DST_BIT |
      VK_BUFFER_USAGE_STORAGE_BUFFER_BIT |
      VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
  for (unsigned int i = 0; i < c_frames_in_flight; ++i) {
    Renderer::createBuffer(buffer_size, buffer_usage_flags,
                           VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                           m_boids_buffer[i], m_boids_buffer_memory[i]);
    Renderer::copyBuffer(staging_buffer, m_boids_buffer[i], buffer_size);
  }

  vkDestroyBuffer(device, staging_buffer, nullptr);
  vkFreeMemory(device, staging_buffer_memory, nullptr);
}

void Boids::updateComputeUniformBuffer(float _delta_time) {
  m_compute_uniform_object.m_delta_time = _delta_time;

  memcpy(m_compute_uniform_buffers_mapped[Renderer::getCurrentFrameInFlight()],
         &m_compute_uniform_object, sizeof(m_compute_uniform_object));
}

void Boids::renderImGui(VkCommandBuffer& _command_buffer) {
  Renderer::beginImGui();
  ImGui::SliderFloat("separation range",
                     &m_compute_uniform_object.m_separation_range,
                     0.0f, 10.0f);
  ImGui::SliderFloat("separation factor",
                     &m_compute_uniform_object.m_separation_factor,
                     0.0f, 10.0f);
  ImGui::SliderFloat("alignment range",
                     &m_compute_uniform_object.m_alignment_range,
                     0.0f, 10.0f);
  ImGui::SliderFloat("alignment factor",
                     &m_compute_uniform_object.m_alignment_factor,
                     0.0f, 10.0f);
  ImGui::SliderFloat("cohesion range",
                     &m_compute_uniform_object.m_cohesion_range,
                     0.0f, 10.0f);
  ImGui::SliderFloat("cohesion factor",
                     &m_compute_uniform_object.m_cohesion_factor,
                     0.0f, 10.0f);
  ImGui::SliderFloat("min speed",
                     &m_compute_uniform_object.m_min_speed,
                     0.0f, 100.0f);
  ImGui::SliderFloat("max speed",
                     &m_compute_uniform_object.m_max_speed,
                     0.0f, 100.0f);
  ImGui::SliderFloat("turn acceleration",
                     &m_compute_uniform_object.m_turn_acceleration,
                     0.0f, 200.0f);
  ImGui::SliderFloat3("box size",
                     (float*) &m_compute_uniform_object.m_box_size,
                     0.0f, 100.0f);
  ImGui::SliderFloat("boids size",
                     &m_boid_size,
                     0.0f, 10.0f);
  Renderer::endImGui(_command_buffer);
}
