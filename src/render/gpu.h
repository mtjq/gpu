#pragma once


#ifdef NDEBUG
const bool enable_validation_layers = false;
#else
const bool enable_validation_layers = true;
#endif


class Gpu {
public:
  struct QueueFamilyIndices {
    std::optional<uint32_t> m_main_family{};
    std::optional<uint32_t> m_present_family{};

    bool isComplete() {
      return (m_main_family.has_value() && m_present_family.has_value());
    }
  };

  struct SwapchainSupportDetails {
    VkSurfaceCapabilitiesKHR m_capabilities{};
    std::vector<VkSurfaceFormatKHR> m_formats{};
    std::vector<VkPresentModeKHR> m_present_modes{};
  };

  Gpu(const Gpu&) = delete;
  Gpu& operator=(const Gpu&) = delete;

  static void init(GLFWwindow* _window);
  static void cleanup();

  static VkInstance& getVkInstance() { return getInstance().m_instance; }
  static VkSurfaceKHR& getSurface() { return getInstance().m_surface; }
  static VkPhysicalDevice& getPhysicalDevice() {
    return getInstance().m_physical_device;
  }
  static VkDevice& getDevice() { return getInstance().m_device; }
  static VkSampleCountFlagBits getMsaaSamples() {
    return getInstance().m_msaa_samples;
  }

  static void getPhysicalDeviceProperties(
      VkPhysicalDeviceProperties* _properties) {
    vkGetPhysicalDeviceProperties(getPhysicalDevice(), _properties);
  }
  static QueueFamilyIndices getQueueFamilies() {
    return getInstance().queryQueueFamilies(getPhysicalDevice());
  }
  static SwapchainSupportDetails getSwapchainSupport() {
    return getInstance().querySwapchainSupport(getPhysicalDevice());
  }
  static uint32_t getSwapchainImageCount();
  static VkExtent2D chooseSwapExtent(
      GLFWwindow* _window, const VkSurfaceCapabilitiesKHR& _capabilities);
  static uint32_t findMemoryType(uint32_t _type_filter,
                                 VkMemoryPropertyFlags _properties);
  static VkFormat findDepthFormat();
  static void getFormatProperties(VkFormat _format,
                                  VkFormatProperties* _properties) {
    vkGetPhysicalDeviceFormatProperties(getPhysicalDevice(),
                                        _format, _properties);
  }

  static void deviceWait() { vkDeviceWaitIdle(getDevice()); }


private:
  VkInstance            m_instance{VK_NULL_HANDLE};
  VkSurfaceKHR          m_surface{VK_NULL_HANDLE};
  VkPhysicalDevice      m_physical_device{VK_NULL_HANDLE};
  VkDevice              m_device{VK_NULL_HANDLE};
  VkSampleCountFlagBits m_msaa_samples{VK_SAMPLE_COUNT_1_BIT};

  Gpu() = default;

  static Gpu& getInstance() {
    static Gpu m_instance;
    return m_instance;
  }

  void createInstance();
  // TODO: setup debug messenger.
  void createSurface(GLFWwindow* _window);
  void pickPhysicalDevice();
  void createLogicalDevice();
  bool isDeviceSuitable(VkPhysicalDevice _device);
  VkSampleCountFlagBits getMaxUsableSampleCount();
  QueueFamilyIndices queryQueueFamilies(VkPhysicalDevice _physical_device);
  SwapchainSupportDetails querySwapchainSupport(
      VkPhysicalDevice _physical_device);
  VkFormat findSupportedFormat(const std::vector<VkFormat>& _candidates,
                               VkImageTiling _tiling,
                               VkFormatFeatureFlags _features);
};
