#include "renderer.h"

#include "imgui/imgui.h"
#include "imgui/backends/imgui_impl_glfw.h"
#include "imgui/backends/imgui_impl_vulkan.h"

#include "core/filesystem_manager.h"
#include "core/utils.h"
#include "render/gpu.h"


unsigned int box_indices[60]{
  // Fill.
  // Front.
  0, 1, 3,
  3, 2, 0,
  // Top.
  2, 3, 7,
  7, 6, 2,
  // Right.
  1, 5, 7,
  7, 3, 1,
  // Bottom.
  4, 5, 1,
  1, 0, 4,
  // Left.
  4, 0, 2,
  2, 6, 4,
  // Back.
  5, 4, 6,
  6, 7, 5,

  // Wire.
  0, 1,
  1, 3,
  3, 2,
  2, 0,
  0, 4,
  1, 5,
  2, 6,
  3, 7,
  4, 5,
  5, 7,
  7, 6,
  6, 4
};

static VkSurfaceFormatKHR chooseSwapSurfaceFormat(
    const std::vector<VkSurfaceFormatKHR>& _available_formats) {
  for (const VkSurfaceFormatKHR& available_format : _available_formats) {
    if (available_format.format == VK_FORMAT_B8G8R8A8_SRGB &&
        available_format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
      return available_format;
    }
  }

  return _available_formats[0];
}

static VkPresentModeKHR chooseSwapPresentMode(
    const std::vector<VkPresentModeKHR>& available_present_modes) {
  for (const VkPresentModeKHR& available_present_mode :
           available_present_modes) {
    if (available_present_mode == VK_PRESENT_MODE_MAILBOX_KHR) {
      return available_present_mode;
    }
  }

  return VK_PRESENT_MODE_FIFO_KHR;
}

void Renderer::beginImGui() {
  ImGuiIO& io = ImGui::GetIO();

  ImGui_ImplVulkan_NewFrame();
  ImGui_ImplGlfw_NewFrame();
  ImGui::NewFrame();

  ImGui::SetNextWindowPos(ImVec2(0.0f, 0.0f), ImGuiCond_FirstUseEver);
  ImGui::SetNextWindowSize(ImVec2(256.0f, 75.0f), ImGuiCond_FirstUseEver);

  ImGui::Begin("Tools");
  ImGui::Text("%.3f ms/frame (%.1f FPS)",
              1000.0f / io.Framerate, io.Framerate);
  if (ImGui::Button("Close")) {
    glfwSetWindowShouldClose(getInstance().m_window, true);
  }
}

void Renderer::endImGui(VkCommandBuffer& _command_buffer) {
  ImGui::End();
  ImGui::Render();
  ImDrawData* imgui_draw_data = ImGui::GetDrawData();
  ImGui_ImplVulkan_RenderDrawData(imgui_draw_data,
                                  _command_buffer);
}

void Renderer::initBoxRendering() {
  getInstance().initBoxRenderingInternal();
}

void Renderer::addBox(const ColouredInstance& _box, bool _wired) {
  Renderer& r = getInstance();
  std::vector<ColouredInstance>& box_container = _wired ? r.m_boxes_wire :
                                                          r.m_boxes;
  if (box_container.size() < c_max_boxes) {
    box_container.push_back(_box);
  } else {
    std::cerr << "Reached the maximum number of boxes, will not be drawn.\n";
  }
}

void Renderer:: renderBoxes(VkCommandBuffer& _command_buffer) {
  Renderer& r = getInstance();

  memcpy(r.m_box_instance_buffer_mapped, r.m_boxes.data(),
         r.m_boxes.size() * sizeof(ColouredInstance));
  memcpy(r.m_box_wire_instance_buffer_mapped, r.m_boxes_wire.data(),
         r.m_boxes_wire.size() * sizeof(ColouredInstance));
  r.updateBoxUniformBuffer(r.m_view_proj);
  r.recordBoxCommandBuffer(_command_buffer, getCurrentFrameInFlight());
}

void Renderer::endFrame(VkCommandBuffer& _command_buffer) {
  getInstance().endFrameInternal(_command_buffer);
}

void Renderer::initInternal(GLFWwindow* _window) {
  m_window = _window;

  createQueues();

  createCommandPool();
  createCommandBuffers();

  createSwapchain();
  createSwapchainImages();
  createSwapchainImageViews();

  createSyncObjects();

  createColourResources();
  createDepthResources();

  createRenderPass();
  createFramebuffers();

  initImGui(_window);
}

void Renderer::cleanupInternal() {
  VkDevice device = Gpu::getDevice();

  cleanupImGui();

  cleanupSwapchain();
  for (size_t i = 0; i < c_frames_in_flight; ++i) {
    vkDestroySemaphore(device, m_swapchain_available_semaphores[i], nullptr);
    vkDestroySemaphore(device, m_render_finished_semaphores[i], nullptr);
    vkDestroyFence(device, m_in_flight_fences[i], nullptr);
  }
  vkDestroyCommandPool(device, m_main_command_pool, nullptr);
  vkDestroyRenderPass(device, m_render_pass, nullptr);

  // ImGui.
  vkDestroyDescriptorPool(device, m_imgui_descriptor_pool, nullptr);

  // Boxes.
  vkDestroyPipeline(device, m_box_pipeline, nullptr);
  vkDestroyPipelineLayout(device, m_box_pipeline_layout, nullptr);
  vkDestroyDescriptorSetLayout(device, m_box_descriptor_set_layout, nullptr);
  vkDestroyDescriptorPool(device, m_box_descriptor_pool, nullptr);
  vkFreeMemory(device, m_box_instance_buffer_memory, nullptr);
  vkDestroyBuffer(device, m_box_instance_buffer, nullptr);
  vkFreeMemory(device, m_box_wire_instance_buffer_memory, nullptr);
  vkDestroyBuffer(device, m_box_wire_instance_buffer, nullptr);
  vkFreeMemory(device, m_box_index_buffer_memory, nullptr);
  vkDestroyBuffer(device, m_box_index_buffer, nullptr);
  for (size_t i = 0; i < c_frames_in_flight; ++i) {
    vkDestroyBuffer(device, m_box_uniform_buffers[i], nullptr);
    vkFreeMemory(device, m_box_uniform_buffers_memories[i], nullptr);
  }
}

VkCommandBuffer& Renderer::beginFrameInternal(const glm::mat4& _view_proj) {
  VkDevice& device = Gpu::getDevice();

  m_view_proj = _view_proj;

  vkWaitForFences(device, 1, &m_in_flight_fences[m_current_frame_in_flight],
                  VK_TRUE, UINT64_MAX);
  vkResetFences(device, 1, &m_in_flight_fences[m_current_frame_in_flight]);

  VkResult acquire_result = vkAcquireNextImageKHR(
      device, m_swapchain, UINT64_MAX,
      m_swapchain_available_semaphores[m_current_frame_in_flight],
      VK_NULL_HANDLE, &m_current_swapchain_image_idx);

  if (acquire_result == VK_ERROR_OUT_OF_DATE_KHR) {
    recreateSwapchain();
    return m_main_command_buffers[m_current_frame_in_flight];
  } else if (acquire_result != VK_SUCCESS &&
             acquire_result != VK_SUBOPTIMAL_KHR) {
    throw std::runtime_error("failed to acquire swap chain image");
  }

  vkResetCommandBuffer(m_main_command_buffers[m_current_frame_in_flight], 0);

  VkCommandBufferBeginInfo begin_info{};
  begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

  VkResult begin_result =
      vkBeginCommandBuffer(m_main_command_buffers[m_current_frame_in_flight],
                           &begin_info);
  if (begin_result != VK_SUCCESS) {
    throw std::runtime_error("failed to begin recording command buffer");
  }

  return m_main_command_buffers[m_current_frame_in_flight];
}

void Renderer::beginRenderPassInternal(VkCommandBuffer& _command_buffer) {
  VkRenderPassBeginInfo render_pass_info{};
  render_pass_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
  render_pass_info.renderPass = m_render_pass;
  render_pass_info.framebuffer = m_framebuffers[m_current_swapchain_image_idx];
  render_pass_info.renderArea.offset = {0, 0};
  render_pass_info.renderArea.extent = m_swapchain_extent;
  std::array<VkClearValue, 2> clear_values;
  clear_values[0].color = {{0.0f, 0.0f, 0.0f, 1.0f}};
  clear_values[1].depthStencil = {1.0f, 0};
  render_pass_info.clearValueCount = clear_values.size();
  render_pass_info.pClearValues = clear_values.data();
  vkCmdBeginRenderPass(_command_buffer, &render_pass_info,
                       VK_SUBPASS_CONTENTS_INLINE);

  VkViewport viewport{};
  viewport.x = 0.0f;
  viewport.y = 0.0f;
  viewport.width = m_swapchain_extent.width;
  viewport.height = m_swapchain_extent.height;
  viewport.minDepth = 0.0f;
  viewport.maxDepth = 1.0f;
  vkCmdSetViewport(_command_buffer, 0, 1, &viewport);

  VkRect2D scissor{};
  scissor.offset = {0, 0};
  scissor.extent = m_swapchain_extent;
  vkCmdSetScissor(_command_buffer, 0, 1, &scissor);
}

void Renderer::endFrameInternal(VkCommandBuffer& _command_buffer) {
  vkEndCommandBuffer(_command_buffer);

  VkSubmitInfo submit_info{};
  submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

  VkPipelineStageFlags wait_stages[] = {
      VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
  submit_info.pWaitDstStageMask = wait_stages;

  submit_info.commandBufferCount = 1;
  submit_info.pCommandBuffers = &_command_buffer;
  submit_info.waitSemaphoreCount = 1;
  submit_info.pWaitSemaphores =
      &m_swapchain_available_semaphores[m_current_frame_in_flight];
  submit_info.signalSemaphoreCount = 1;
  submit_info.pSignalSemaphores =
      &m_render_finished_semaphores[m_current_frame_in_flight];

  VkResult submit_result =
      vkQueueSubmit(m_main_queue, 1, &submit_info,
                    m_in_flight_fences[m_current_frame_in_flight]);
  if (submit_result != VK_SUCCESS) {
    throw std::runtime_error("failed to submit draw command buffer");
  }

  VkPresentInfoKHR present_info{};
  present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

  present_info.waitSemaphoreCount = 1;
  present_info.pWaitSemaphores =
      &m_render_finished_semaphores[m_current_frame_in_flight];
  present_info.swapchainCount = 1;
  present_info.pSwapchains = &m_swapchain;
  present_info.pImageIndices = &m_current_swapchain_image_idx;

  VkResult present_result = vkQueuePresentKHR(m_present_queue, &present_info);

  if (present_result == VK_ERROR_OUT_OF_DATE_KHR ||
      present_result == VK_SUBOPTIMAL_KHR ||
      m_framebuffer_size_dirty) {
    m_framebuffer_size_dirty = false;
    recreateSwapchain();
  } else if (present_result != VK_SUCCESS) {
    throw std::runtime_error("failed to present swap chain image");
  }

  m_current_frame_in_flight =
      (m_current_frame_in_flight + 1) % c_frames_in_flight;
}

void Renderer::createQueues() {
  VkDevice& device = Gpu::getDevice();
  Gpu::QueueFamilyIndices queue_family_indices = Gpu::getQueueFamilies();

  vkGetDeviceQueue(device, queue_family_indices.m_present_family.value(), 0,
                   &m_present_queue);
  vkGetDeviceQueue(device, queue_family_indices.m_main_family.value(), 0,
                   &m_main_queue);
}

void Renderer::createCommandPool() {
  Gpu::QueueFamilyIndices queue_family_indices = Gpu::getQueueFamilies();

  VkCommandPoolCreateInfo pool_info{};
  pool_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
  pool_info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
  pool_info.queueFamilyIndex = queue_family_indices.m_main_family.value();

  VkResult create_pool_result = vkCreateCommandPool(Gpu::getDevice(),
                                                    &pool_info, nullptr,
                                                    &m_main_command_pool);
  if (create_pool_result != VK_SUCCESS) {
    throw std::runtime_error("failed to create main command pool");
  }
}

void Renderer::createCommandBuffers() {
  VkCommandBufferAllocateInfo alloc_info{};
  alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  alloc_info.commandPool = m_main_command_pool;
  alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  alloc_info.commandBufferCount = m_main_command_buffers.size();

  VkResult allocate_result =
      vkAllocateCommandBuffers(Gpu::getDevice(), &alloc_info,
                               m_main_command_buffers.data());
  if (allocate_result != VK_SUCCESS) {
    throw std::runtime_error("failed to allocate main command buffers");
  }
}

void Renderer::createSwapchain() {
  Gpu::SwapchainSupportDetails swapchain_support = Gpu::getSwapchainSupport();

  VkSurfaceFormatKHR surface_format =
      chooseSwapSurfaceFormat(swapchain_support.m_formats);
  VkPresentModeKHR present_mode =
      chooseSwapPresentMode(swapchain_support.m_present_modes);
  VkExtent2D extent = Gpu::chooseSwapExtent(m_window,
                                            swapchain_support.m_capabilities);

  unsigned int image_count = Gpu::getSwapchainImageCount();

  VkSwapchainCreateInfoKHR create_info{};
  create_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
  create_info.surface = Gpu::getSurface();
  create_info.minImageCount = image_count;
  create_info.imageFormat = surface_format.format;
  create_info.imageColorSpace = surface_format.colorSpace;
  create_info.imageExtent = extent;
  create_info.imageArrayLayers = 1;
  create_info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
  create_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
  create_info.preTransform = swapchain_support.m_capabilities.currentTransform;
  create_info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
  create_info.presentMode = present_mode;
  // Might have some downsides, like in SSR for instance?
  create_info.clipped = VK_TRUE;
  create_info.oldSwapchain = VK_NULL_HANDLE;

  VkResult create_result = vkCreateSwapchainKHR(Gpu::getDevice(), &create_info,
                                                nullptr, &m_swapchain);
  if (create_result != VK_SUCCESS) {
    throw std::runtime_error("failed to create swap chain");
  }

  m_swapchain_image_format = surface_format.format;
  m_swapchain_extent = extent;
}

void Renderer::createSwapchainImages() {
  VkDevice& device = Gpu::getDevice();

  unsigned int image_count = Gpu::getSwapchainImageCount();

  vkGetSwapchainImagesKHR(device, m_swapchain, &image_count, nullptr);
  m_swapchain_images.resize(image_count);
  vkGetSwapchainImagesKHR(device, m_swapchain, &image_count,
                          m_swapchain_images.data());
}

void Renderer::createSwapchainImageViews() {
  m_swapchain_image_views.resize(m_swapchain_images.size());

  for (size_t i = 0; i < m_swapchain_images.size(); ++i) {
    m_swapchain_image_views[i] = createImageView(m_swapchain_images[i],
                                                 m_swapchain_image_format,
                                                 VK_IMAGE_ASPECT_COLOR_BIT);
  }
}

void Renderer::createRenderPass() {
  VkAttachmentDescription colour_attachment{};
  colour_attachment.format = m_swapchain_image_format;
  colour_attachment.samples = Gpu::getMsaaSamples();
  colour_attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
  colour_attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
  colour_attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  colour_attachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

  VkAttachmentReference colour_attachment_ref{};
  colour_attachment_ref.attachment = 0;
  colour_attachment_ref.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

  VkAttachmentDescription depth_attachment{};
  depth_attachment.format = Gpu::findDepthFormat();
  depth_attachment.samples = Gpu::getMsaaSamples();
  depth_attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
  depth_attachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
  depth_attachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
  depth_attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
  depth_attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  depth_attachment.finalLayout =
      VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

  VkAttachmentReference depth_attachment_ref{};
  depth_attachment_ref.attachment = 1;
  depth_attachment_ref.layout =
      VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

  VkAttachmentDescription colour_attachment_resolve{};
  colour_attachment_resolve.format = m_swapchain_image_format;
  colour_attachment_resolve.samples = VK_SAMPLE_COUNT_1_BIT;
  colour_attachment_resolve.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
  colour_attachment_resolve.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
  colour_attachment_resolve.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  colour_attachment_resolve.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

  VkAttachmentReference colour_attachment_resolve_ref{};
  colour_attachment_resolve_ref.attachment = 2;
  colour_attachment_resolve_ref.layout =
      VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

  VkSubpassDescription subpass{};
  subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
  subpass.colorAttachmentCount = 1;
  subpass.pColorAttachments = &colour_attachment_ref;
  subpass.pDepthStencilAttachment = &depth_attachment_ref;
  subpass.pResolveAttachments = &colour_attachment_resolve_ref;

  std::array<VkAttachmentDescription, 3> attachments = {
      colour_attachment, depth_attachment, colour_attachment_resolve};

  VkRenderPassCreateInfo render_pass_info{};
  render_pass_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
  render_pass_info.attachmentCount = attachments.size();
  render_pass_info.pAttachments = attachments.data();
  render_pass_info.subpassCount = 1;
  render_pass_info.pSubpasses = &subpass;

  VkSubpassDependency dependency{};
  dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
  dependency.dstSubpass = 0;
  dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT |
                            VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
  dependency.srcAccessMask = 0;
  dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT |
                            VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
  dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT |
                             VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

  render_pass_info.dependencyCount = 1;
  render_pass_info.pDependencies = &dependency;

  VkResult create_result = vkCreateRenderPass(Gpu::getDevice(),
                                              &render_pass_info,
                                              nullptr, &m_render_pass);
  if (create_result != VK_SUCCESS) {
    throw std::runtime_error("failed to create render pass");
  }
}

void Renderer::createSyncObjects() {
  VkDevice& device = Gpu::getDevice();

  VkSemaphoreCreateInfo semaphore_info{};
  semaphore_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

  VkFenceCreateInfo fence_info{};
  fence_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
  fence_info.flags = VK_FENCE_CREATE_SIGNALED_BIT;

  VkResult create_result;
  for (size_t i = 0; i < c_frames_in_flight; ++i) {
    create_result = vkCreateSemaphore(device, &semaphore_info, nullptr,
                                      &m_swapchain_available_semaphores[i]);
    if (create_result != VK_SUCCESS) {
      throw std::runtime_error("failed to create semaphore");
    }

    create_result = vkCreateSemaphore(device, &semaphore_info, nullptr,
                                      &m_render_finished_semaphores[i]);
    if (create_result != VK_SUCCESS) {
      throw std::runtime_error("failed to create semaphore");
    }

    create_result = vkCreateFence(device, &fence_info, nullptr,
                                  &m_in_flight_fences[i]);
    if (create_result != VK_SUCCESS) {
      throw std::runtime_error("failed to create fence");
    }
  }
}

void Renderer::createFramebuffers() {
  m_framebuffers.resize(m_swapchain_image_views.size());
  for (size_t i = 0; i < m_swapchain_image_views.size(); ++i) {
    std::array<VkImageView, 3> attachments = {m_colour_image_view,
                                              m_depth_image_view,
                                              m_swapchain_image_views[i]};

    VkFramebufferCreateInfo framebuffer_info{};
    framebuffer_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    framebuffer_info.renderPass = m_render_pass;
    framebuffer_info.attachmentCount = attachments.size();
    framebuffer_info.pAttachments = attachments.data();
    framebuffer_info.width = m_swapchain_extent.width;
    framebuffer_info.height = m_swapchain_extent.height;
    framebuffer_info.layers = 1;

    VkResult create_framebuffer_result =
        vkCreateFramebuffer(Gpu::getDevice(), &framebuffer_info, nullptr,
                            &m_framebuffers[i]);
    if (create_framebuffer_result != VK_SUCCESS) {
      throw std::runtime_error("failed to create framebuffer");
    }
  }
}

void Renderer::createColourResources() {
  VkFormat colour_format = m_swapchain_image_format;

  VkImageUsageFlags usage = VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT |
                            VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
  createImage(m_swapchain_extent.width, m_swapchain_extent.height,
              colour_format, VK_IMAGE_TILING_OPTIMAL, usage,
              VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, m_colour_image,
              m_colour_image_memory, 1, Gpu::getMsaaSamples());
  m_colour_image_view = createImageView(m_colour_image, colour_format,
                                        VK_IMAGE_ASPECT_COLOR_BIT);
}

void Renderer::createDepthResources() {
  VkFormat depth_format = Gpu::findDepthFormat();

  createImage(m_swapchain_extent.width, m_swapchain_extent.height,
              depth_format, VK_IMAGE_TILING_OPTIMAL,
              VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
              VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
              m_depth_image, m_depth_image_memory,
              1, Gpu::getMsaaSamples());
  m_depth_image_view = createImageView(m_depth_image, depth_format,
                                       VK_IMAGE_ASPECT_DEPTH_BIT);
}

VkShaderModule Renderer::createShaderModule(const std::vector<char>& _code) {
  VkShaderModuleCreateInfo create_info{};
  create_info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
  create_info.codeSize = _code.size();
  create_info.pCode = reinterpret_cast<const uint32_t*>(_code.data());

  VkShaderModule shader_module;
  VkResult create_module_result = vkCreateShaderModule(Gpu::getDevice(),
                                                       &create_info,
                                                       nullptr, &shader_module);
  if (create_module_result != VK_SUCCESS) {
    throw std::runtime_error("failed to create shader module");
  }

  return shader_module;
}

void Renderer::createBuffer(VkDeviceSize _size, VkBufferUsageFlags _usage,
                            VkMemoryPropertyFlags _properties,
                            VkBuffer& _buffer, VkDeviceMemory& _bufferMemory) {
  VkDevice& device = Gpu::getDevice();

  VkBufferCreateInfo buffer_info{};
  buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
  buffer_info.size = _size;
  buffer_info.usage = _usage;

  buffer_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

  VkResult create_buffer_result = vkCreateBuffer(device, &buffer_info,
                                                 nullptr, &_buffer);
  if (create_buffer_result != VK_SUCCESS) {
    throw std::runtime_error("failed to create buffer");
  }

  VkMemoryRequirements mem_requirements;
  vkGetBufferMemoryRequirements(device, _buffer, &mem_requirements);

  VkMemoryAllocateInfo alloc_info{};
  alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
  alloc_info.allocationSize = mem_requirements.size;
  alloc_info.memoryTypeIndex =
      Gpu::findMemoryType(mem_requirements.memoryTypeBits, _properties);

  VkResult allocate_result = vkAllocateMemory(device, &alloc_info, nullptr,
                                              &_bufferMemory);
  if (allocate_result != VK_SUCCESS) {
    throw std::runtime_error("failed to allocate buffer memory");
  }

  vkBindBufferMemory(device, _buffer, _bufferMemory, 0);
}

void Renderer::copyBuffer(VkBuffer _srcBuffer, VkBuffer _dstBuffer,
                          VkDeviceSize _size) {
  VkCommandBuffer command_buffer = beginSingleTimeCommands();

  VkBufferCopy copy_region{};
  copy_region.srcOffset = 0;
  copy_region.dstOffset = 0;
  copy_region.size = _size;
  vkCmdCopyBuffer(command_buffer, _srcBuffer, _dstBuffer, 1, &copy_region);

  endSingleTimeCommands(command_buffer);
}

void Renderer::createImage(int _width, int _height, VkFormat _format,
                           VkImageTiling _tiling, VkImageUsageFlags _usage,
                           VkMemoryPropertyFlags _properties, VkImage& _image,
                           VkDeviceMemory& _image_memory,
                           unsigned int _mip_levels,
                           VkSampleCountFlagBits _sample_count) {
  VkDevice& device = Gpu::getDevice();

  VkImageCreateInfo image_info{};
  image_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
  image_info.imageType = VK_IMAGE_TYPE_2D;
  image_info.extent.width = _width;
  image_info.extent.height = _height;
  image_info.extent.depth = 1;
  image_info.mipLevels = _mip_levels;
  image_info.arrayLayers = 1;
  image_info.format = _format;
  image_info.tiling = _tiling;
  image_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  image_info.usage = _usage;

  image_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

  image_info.samples = _sample_count;
  image_info.flags = 0;

  VkResult create_result = vkCreateImage(device, &image_info, nullptr,
                                         &_image);
  if (create_result != VK_SUCCESS) {
    throw std::runtime_error("failed to create image");
  }

  VkMemoryRequirements mem_requirements;
  vkGetImageMemoryRequirements(device, _image, &mem_requirements);

  VkMemoryAllocateInfo alloc_info{};
  alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
  alloc_info.allocationSize = mem_requirements.size;
  alloc_info.memoryTypeIndex =
      Gpu::findMemoryType(mem_requirements.memoryTypeBits, _properties);

  VkResult allocate_result = vkAllocateMemory(device, &alloc_info, nullptr,
                                              &_image_memory);
  if (allocate_result != VK_SUCCESS) {
    throw std::runtime_error("failed to allocate image memory");
  }

  vkBindImageMemory(device, _image, _image_memory, 0);
}

VkImageView Renderer::createImageView(VkImage _image, VkFormat _format,
                                      VkImageAspectFlags _aspect_flags,
                                      unsigned int _mip_levels) {
  VkImageViewCreateInfo view_info{};
  view_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
  view_info.image = _image;
  view_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
  view_info.format = _format;
  view_info.subresourceRange.aspectMask = _aspect_flags;
  view_info.subresourceRange.baseMipLevel = 0;
  view_info.subresourceRange.levelCount = _mip_levels;
  view_info.subresourceRange.baseArrayLayer = 0;
  view_info.subresourceRange.layerCount = 1;

  VkImageView image_view;
  VkResult create_result = vkCreateImageView(Gpu::getDevice(),
                                             &view_info, nullptr, &image_view);
  if (create_result != VK_SUCCESS) {
    throw std::runtime_error("failed to create texture image view");
  }

  return image_view;
}

VkCommandBuffer Renderer::beginSingleTimeCommands() {
  VkCommandBufferAllocateInfo alloc_info{};
  alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  alloc_info.commandPool = getInstance().m_main_command_pool;
  alloc_info.commandBufferCount = 1;

  VkCommandBuffer command_buffer;
  vkAllocateCommandBuffers(Gpu::getDevice(), &alloc_info,
                           &command_buffer);

  VkCommandBufferBeginInfo begin_info{};
  begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
  begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

  vkBeginCommandBuffer(command_buffer, &begin_info);

  return command_buffer;
}

void Renderer::endSingleTimeCommands(VkCommandBuffer _command_buffer) {
  vkEndCommandBuffer(_command_buffer);

  VkSubmitInfo submit_info{};
  submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submit_info.commandBufferCount = 1;
  submit_info.pCommandBuffers = &_command_buffer;

  vkQueueSubmit(getInstance().m_main_queue, 1, &submit_info, VK_NULL_HANDLE);
  vkQueueWaitIdle(getInstance().m_main_queue);

  vkFreeCommandBuffers(Gpu::getDevice(), getInstance().m_main_command_pool, 1,
                       &_command_buffer);
}

void Renderer::cleanupSwapchain() {
  VkDevice& device = Gpu::getDevice();

  vkDestroyImageView(device, m_colour_image_view, nullptr);
  vkDestroyImage(device, m_colour_image, nullptr);
  vkFreeMemory(device, m_colour_image_memory, nullptr);
  vkDestroyImageView(device, m_depth_image_view, nullptr);
  vkDestroyImage(device, m_depth_image, nullptr);
  vkFreeMemory(device, m_depth_image_memory, nullptr);
  for (VkFramebuffer framebuffer : m_framebuffers) {
    vkDestroyFramebuffer(device, framebuffer, nullptr);
  }
  for (VkImageView image_view : m_swapchain_image_views) {
    vkDestroyImageView(device, image_view, nullptr);
  }
  vkDestroySwapchainKHR(device, m_swapchain, nullptr);
}

void Renderer::recreateSwapchain() {
  int width = 0;
  int height = 0;
  glfwGetFramebufferSize(m_window, &width, &height);
  while (width == 0 || height == 0) {
    glfwGetFramebufferSize(m_window, &width, &height);
    glfwWaitEvents();
  }

  vkDeviceWaitIdle(Gpu::getDevice());

  // TODO: use old swap chain for drawing still in-flight.
  // https://vulkan-tutorial.com/en/Drawing_a_triangle/Swap_chain_recreation
  cleanupSwapchain();

  createSwapchain();
  createSwapchainImages();
  createSwapchainImageViews();

  createColourResources();
  createDepthResources();
  createFramebuffers();
}

// ImGui.
void Renderer::initImGui(GLFWwindow* _window) {
  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImGuiIO& io = ImGui::GetIO(); (void)io;
  io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
  io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;

  ImGui::StyleColorsDark();

  VkDescriptorPoolSize pool_sizes[] =
  {
    { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1 },
  };
  VkDescriptorPoolCreateInfo pool_info = {};
  pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
  pool_info.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
  pool_info.maxSets = 1;
  pool_info.poolSizeCount = (uint32_t) IM_ARRAYSIZE(pool_sizes);
  pool_info.pPoolSizes = pool_sizes;
  VkResult create_pool_result =
      vkCreateDescriptorPool(Gpu::getDevice(), &pool_info, nullptr,
                             &m_imgui_descriptor_pool);
  if (create_pool_result != VK_SUCCESS) {
    throw std::runtime_error("failed to create imgui command pool");
  }

  ImGui_ImplGlfw_InitForVulkan(_window, true);
  ImGui_ImplVulkan_InitInfo init_info = {};
  init_info.Instance = Gpu::getVkInstance();
  init_info.PhysicalDevice = Gpu::getPhysicalDevice();
  init_info.Device = Gpu::getDevice();
  init_info.QueueFamily = Gpu::getQueueFamilies().m_main_family.value();
  init_info.Queue = m_main_queue;
  init_info.PipelineCache = VK_NULL_HANDLE;
  init_info.DescriptorPool = m_imgui_descriptor_pool;
  init_info.RenderPass = m_render_pass;
  init_info.Subpass = 0;
  init_info.MinImageCount = c_frames_in_flight;
  init_info.ImageCount = c_frames_in_flight;
  init_info.MSAASamples = Gpu::getMsaaSamples();
  init_info.Allocator = nullptr;
  init_info.CheckVkResultFn = nullptr;
  ImGui_ImplVulkan_Init(&init_info);
}

void Renderer::cleanupImGui() {
  ImGui_ImplVulkan_Shutdown();
  ImGui_ImplGlfw_Shutdown();
  ImGui::DestroyContext();
}

// Boxes.
void Renderer::initBoxRenderingInternal() {
  createBoxVertexBuffers();
  createBoxIndexBuffers();
  createBoxUniformBuffers();
  createBoxDescriptorPool();
  createBoxDescriptorSetLayout();
  createBoxDescriptorSets();
  createBoxPipeline();
}

void Renderer::createBoxPipeline() {
  VkDevice device = Gpu::getDevice();

  std::vector<char> vert_shader_code = readFile(
      FilesystemManager::FromRootDirRelativeToAbsolute(
          "assets/shaders/box_vert.spv"));
  std::vector<char> frag_shader_code = readFile(
      FilesystemManager::FromRootDirRelativeToAbsolute(
          "assets/shaders/box_frag.spv"));

  VkShaderModule vert_shader_module = createShaderModule(vert_shader_code);
  VkShaderModule frag_shader_module = createShaderModule(frag_shader_code);

  VkPipelineShaderStageCreateInfo vert_shader_stage_info{};
  vert_shader_stage_info.sType =
      VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
  vert_shader_stage_info.stage = VK_SHADER_STAGE_VERTEX_BIT;
  vert_shader_stage_info.module = vert_shader_module;
  vert_shader_stage_info.pName = "main";

  VkPipelineShaderStageCreateInfo frag_shader_stage_info{};
  frag_shader_stage_info.sType =
      VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
  frag_shader_stage_info.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
  frag_shader_stage_info.module = frag_shader_module;
  frag_shader_stage_info.pName = "main";

  VkPipelineShaderStageCreateInfo shader_stages[] = {vert_shader_stage_info,
                                                     frag_shader_stage_info};

  VkVertexInputBindingDescription binding_description =
      ColouredInstance::getBindingDescription();
  auto attribute_descriptions = ColouredInstance::getAttributeDescriptions();
  VkPipelineVertexInputStateCreateInfo vertex_input_info{};
  vertex_input_info.sType =
      VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
  vertex_input_info.vertexBindingDescriptionCount = 1;
  vertex_input_info.pVertexBindingDescriptions = &binding_description;
  vertex_input_info.vertexAttributeDescriptionCount =
      attribute_descriptions.size();
  vertex_input_info.pVertexAttributeDescriptions =
      attribute_descriptions.data();

  VkPipelineInputAssemblyStateCreateInfo input_assembly{};
  input_assembly.sType =
      VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
  input_assembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
  input_assembly.primitiveRestartEnable = VK_FALSE;

  VkViewport viewport{};
  viewport.x = 0.0f;
  viewport.y = 0.0f;
  viewport.width = (float) m_swapchain_extent.width;
  viewport.height = (float) m_swapchain_extent.height;
  viewport.minDepth = 0.0f;
  viewport.maxDepth = 1.0f;

  VkRect2D scissor{};
  scissor.offset = {0, 0};
  scissor.extent = m_swapchain_extent;

  VkPipelineViewportStateCreateInfo viewport_state{};
  viewport_state.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
  viewport_state.viewportCount = 1;
  viewport_state.pViewports = &viewport;
  viewport_state.scissorCount = 1;
  viewport_state.pScissors = &scissor;

  std::array<VkDynamicState, 5> dynamic_states = {
      VK_DYNAMIC_STATE_VIEWPORT,
      VK_DYNAMIC_STATE_SCISSOR,
      VK_DYNAMIC_STATE_PRIMITIVE_TOPOLOGY,
      VK_DYNAMIC_STATE_POLYGON_MODE_EXT,
      VK_DYNAMIC_STATE_DEPTH_WRITE_ENABLE};

  VkPipelineDynamicStateCreateInfo dynamic_state{};
  dynamic_state.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
  dynamic_state.dynamicStateCount = dynamic_states.size();
  dynamic_state.pDynamicStates = dynamic_states.data();

  VkPipelineRasterizationStateCreateInfo rasterizer{};
  rasterizer.sType =
      VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
  rasterizer.depthClampEnable = VK_FALSE;
  rasterizer.rasterizerDiscardEnable = VK_FALSE;
  rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
  rasterizer.lineWidth = 1.0f;
  rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
  rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
  rasterizer.depthBiasEnable = VK_FALSE;

  VkPipelineMultisampleStateCreateInfo multisampling{};
  multisampling.sType =
      VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
  multisampling.sampleShadingEnable = VK_TRUE;
  multisampling.minSampleShading = 0.2f;
  multisampling.rasterizationSamples = Gpu::getMsaaSamples();

  VkPipelineColorBlendAttachmentState colour_blend_attachment{};
  colour_blend_attachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT |
                                           VK_COLOR_COMPONENT_G_BIT |
                                           VK_COLOR_COMPONENT_B_BIT |
                                           VK_COLOR_COMPONENT_A_BIT;
  colour_blend_attachment.blendEnable = VK_TRUE;
  colour_blend_attachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
  colour_blend_attachment.dstColorBlendFactor =
      VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
  colour_blend_attachment.colorBlendOp = VK_BLEND_OP_ADD;
  colour_blend_attachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
  colour_blend_attachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
  colour_blend_attachment.alphaBlendOp = VK_BLEND_OP_ADD;

  VkPipelineColorBlendStateCreateInfo colour_blending{};
  colour_blending.sType =
      VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
  colour_blending.logicOpEnable = VK_FALSE;
  colour_blending.logicOp = VK_LOGIC_OP_COPY;
  colour_blending.attachmentCount = 1;
  colour_blending.pAttachments = &colour_blend_attachment;
  colour_blending.blendConstants[0] = 0.0f;
  colour_blending.blendConstants[1] = 0.0f;
  colour_blending.blendConstants[2] = 0.0f;
  colour_blending.blendConstants[3] = 0.0f;

  VkPipelineLayoutCreateInfo pipeline_layout_info{};
  pipeline_layout_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
  pipeline_layout_info.setLayoutCount = 1;
  pipeline_layout_info.pSetLayouts = &m_box_descriptor_set_layout;

  VkResult create_layout_result =
      vkCreatePipelineLayout(device, &pipeline_layout_info, nullptr,
                             &m_box_pipeline_layout);
  if (create_layout_result != VK_SUCCESS) {
    throw std::runtime_error("failed to create box pipeline layout");
  }

  VkPipelineDepthStencilStateCreateInfo depth_stencil{};
  depth_stencil.sType =
      VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
  depth_stencil.depthTestEnable = VK_TRUE;
  depth_stencil.depthWriteEnable = VK_TRUE;
  depth_stencil.depthCompareOp = VK_COMPARE_OP_LESS;
  depth_stencil.depthBoundsTestEnable = VK_FALSE;
  depth_stencil.stencilTestEnable = VK_FALSE;

  VkGraphicsPipelineCreateInfo pipeline_info{};
  pipeline_info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
  pipeline_info.stageCount = 2;
  pipeline_info.pStages = shader_stages;
  pipeline_info.pVertexInputState = &vertex_input_info;
  pipeline_info.pInputAssemblyState = &input_assembly;
  pipeline_info.pViewportState = &viewport_state;
  pipeline_info.pRasterizationState = &rasterizer;
  pipeline_info.pMultisampleState = &multisampling;
  pipeline_info.pDepthStencilState = &depth_stencil;
  pipeline_info.pColorBlendState = &colour_blending;
  pipeline_info.pDynamicState = &dynamic_state;
  pipeline_info.layout = m_box_pipeline_layout;
  pipeline_info.renderPass = m_render_pass;
  pipeline_info.subpass = 0;

  VkResult create_pipeline_result = vkCreateGraphicsPipelines(device,
                                                              VK_NULL_HANDLE, 1,
                                                              &pipeline_info,
                                                              nullptr,
                                                              &m_box_pipeline);
  if (create_pipeline_result != VK_SUCCESS) {
    throw std::runtime_error("failed to create box render pipeline");
  }

  vkDestroyShaderModule(device, vert_shader_module, nullptr);
  vkDestroyShaderModule(device, frag_shader_module, nullptr);
}

void Renderer::createBoxDescriptorPool() {
  VkDescriptorPoolSize pool_size;
  pool_size.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
  pool_size.descriptorCount = c_frames_in_flight;

  VkDescriptorPoolCreateInfo pool_info{};
  pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
  pool_info.poolSizeCount = 1;
  pool_info.pPoolSizes = &pool_size;
  pool_info.maxSets = c_frames_in_flight;

  VkResult create_result = vkCreateDescriptorPool(Gpu::getDevice(), &pool_info,
                                                  nullptr,
                                                  &m_box_descriptor_pool);
  if (create_result != VK_SUCCESS) {
    throw std::runtime_error("failed to create box descriptor pool");
  }
}

void Renderer::createBoxDescriptorSetLayout() {
  VkDescriptorSetLayoutBinding ubo_layout_binding{};
  ubo_layout_binding.binding = 0;
  ubo_layout_binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
  ubo_layout_binding.descriptorCount = 1;
  ubo_layout_binding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
  ubo_layout_binding.pImmutableSamplers = nullptr;

  VkDescriptorSetLayoutCreateInfo layout_info{};
  layout_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
  layout_info.bindingCount = 1;
  layout_info.pBindings = &ubo_layout_binding;

  VkResult create_result =
      vkCreateDescriptorSetLayout(Gpu::getDevice(), &layout_info, nullptr,
                                  &m_box_descriptor_set_layout);
  if (create_result != VK_SUCCESS) {
    throw std::runtime_error("failed to create box descriptor set layout");
  }
}

void Renderer::createBoxDescriptorSets() {
  VkDevice& device = Gpu::getDevice();

  std::array<VkDescriptorSetLayout, c_frames_in_flight> layouts;
  layouts.fill(m_box_descriptor_set_layout);
  VkDescriptorSetAllocateInfo alloc_info{};
  alloc_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
  alloc_info.descriptorPool = m_box_descriptor_pool;
  alloc_info.descriptorSetCount = c_frames_in_flight;
  alloc_info.pSetLayouts = layouts.data();

  VkResult allocate_result =
      vkAllocateDescriptorSets(device, &alloc_info,
                               m_box_descriptor_sets.data());
  if (allocate_result != VK_SUCCESS) {
    throw std::runtime_error("failed to allocate box descriptor sets");
  }

  for (size_t i = 0; i < c_frames_in_flight; ++i) {
    VkDescriptorBufferInfo buffer_info{};
    buffer_info.buffer = m_box_uniform_buffers[i];
    buffer_info.offset = 0;
    buffer_info.range = sizeof(glm::mat4);

    VkWriteDescriptorSet descriptor_write;
    descriptor_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptor_write.dstSet = m_box_descriptor_sets[i];
    descriptor_write.dstBinding = 0;
    descriptor_write.dstArrayElement = 0;
    descriptor_write.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptor_write.descriptorCount = 1;
    descriptor_write.pBufferInfo = &buffer_info;
    descriptor_write.pNext = nullptr;

    vkUpdateDescriptorSets(device, 1, &descriptor_write, 0, nullptr);
  }
}

void Renderer::createBoxVertexBuffers() {
  VkDeviceSize buffer_size = c_max_boxes * sizeof(ColouredInstance);

  VkMemoryPropertyFlags memory_properties =
      VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
      VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

  createBuffer(buffer_size, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
               memory_properties,
               m_box_instance_buffer, m_box_instance_buffer_memory);
  vkMapMemory(Gpu::getDevice(), m_box_instance_buffer_memory,
              0, buffer_size, 0, &m_box_instance_buffer_mapped);

  createBuffer(buffer_size, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
               memory_properties,
               m_box_wire_instance_buffer, m_box_wire_instance_buffer_memory);
  vkMapMemory(Gpu::getDevice(), m_box_wire_instance_buffer_memory,
              0, buffer_size, 0, &m_box_wire_instance_buffer_mapped);
}

void Renderer::createBoxIndexBuffers() {
  VkDevice& device = Gpu::getDevice();

  VkDeviceSize buffer_size = sizeof(box_indices);

  VkBuffer staging_buffer;
  VkDeviceMemory staging_buffer_memory;
  VkMemoryPropertyFlags memory_properties =
      VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
      VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
  createBuffer(buffer_size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
               memory_properties, staging_buffer, staging_buffer_memory);
  void* data;
  vkMapMemory(device, staging_buffer_memory, 0, buffer_size, 0, &data);
  memcpy(data, box_indices, buffer_size);
  vkUnmapMemory(device, staging_buffer_memory);

  VkBufferUsageFlags usage_flags = VK_BUFFER_USAGE_TRANSFER_DST_BIT |
                                   VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
  createBuffer(buffer_size, usage_flags, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
               m_box_index_buffer, m_box_index_buffer_memory);

  copyBuffer(staging_buffer, m_box_index_buffer, buffer_size);

  vkDestroyBuffer(device, staging_buffer, nullptr);
  vkFreeMemory(device, staging_buffer_memory, nullptr);
}

void Renderer::createBoxUniformBuffers() {
  VkDeviceSize buffer_size = sizeof(glm::mat4);

  for (size_t i = 0; i < c_frames_in_flight; ++i) {
    VkMemoryPropertyFlags memory_properties =
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
        VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
    createBuffer(buffer_size, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                 memory_properties,
                 m_box_uniform_buffers[i], m_box_uniform_buffers_memories[i]);

  vkMapMemory(Gpu::getDevice(), m_box_uniform_buffers_memories[i],
              0, buffer_size, 0, &m_box_uniform_buffers_mapped[i]);
  }
}

static VkResult cmdSetPolygonModeEXT(VkCommandBuffer _command_buffer,
                                     VkPolygonMode _polygon_mode) {
  auto func = (PFN_vkCmdSetPolygonModeEXT) vkGetInstanceProcAddr(
      Gpu::getVkInstance(), "vkCmdSetPolygonModeEXT");
  if (func == nullptr) {
    return VK_ERROR_UNKNOWN;
  }

  func(_command_buffer, _polygon_mode);
  return VK_SUCCESS;
}

static VkResult cmdSetPrimitiveTopologyEXT(VkCommandBuffer _command_buffer,
                                       VkPrimitiveTopology _topology) {
  auto func = (PFN_vkCmdSetPrimitiveTopologyEXT) vkGetInstanceProcAddr(
      Gpu::getVkInstance(), "vkCmdSetPrimitiveTopologyEXT");
  if (func == nullptr) {
    return VK_ERROR_UNKNOWN;
  }

  func(_command_buffer, _topology);
  return VK_SUCCESS;
}

static VkResult cmdSetDepthWriteEnableEXT(VkCommandBuffer _command_buffer,
                                      VkBool32 _bool) {
  auto func = (PFN_vkCmdSetDepthWriteEnableEXT) vkGetInstanceProcAddr(
      Gpu::getVkInstance(), "vkCmdSetDepthWriteEnableEXT");
  if (func == nullptr) {
    return VK_ERROR_UNKNOWN;
  }

  func(_command_buffer, _bool);
  return VK_SUCCESS;
}

void Renderer::recordBoxCommandBuffer(VkCommandBuffer _command_buffer,
                                      unsigned int _current_frame) {
  vkCmdBindPipeline(_command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
                    m_box_pipeline);
  vkCmdBindDescriptorSets(_command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
                          m_box_pipeline_layout,
                          0, 1, &m_box_descriptor_sets[_current_frame],
                          0, nullptr);
  vkCmdBindIndexBuffer(_command_buffer, m_box_index_buffer, 0,
                       VK_INDEX_TYPE_UINT32);

  VkResult vk_result;
  vk_result = cmdSetPrimitiveTopologyEXT(_command_buffer,
                                         VK_PRIMITIVE_TOPOLOGY_LINE_LIST);
  if (vk_result != VK_SUCCESS) {
    std::cerr << "Could not find proc address for vkCmdSetPrimitiveTopologyEXT."
              << std::endl;
    return;
  }
  cmdSetPolygonModeEXT(_command_buffer, VK_POLYGON_MODE_LINE);
  if (vk_result != VK_SUCCESS) {
    std::cerr << "Could not find proc address for vkCmdSetPolygonModeEXT."
              << std::endl;
    return;
  }
  cmdSetDepthWriteEnableEXT(_command_buffer, VK_FALSE);
  if (vk_result != VK_SUCCESS) {
    std::cerr << "Could not find proc address for vkCmdSetWriteEnableEXT."
              << std::endl;
    return;
  }
  VkBuffer vertex_buffers_wire[] = {m_box_wire_instance_buffer};
  VkDeviceSize offsets_wire[] = {0};
  vkCmdBindVertexBuffers(_command_buffer, 0, 1,
                         vertex_buffers_wire, offsets_wire);
  vkCmdDrawIndexed(_command_buffer, 24, m_boxes_wire.size(), 36, 0, 0);

  cmdSetPrimitiveTopologyEXT(_command_buffer,
                             VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);
  if (vk_result != VK_SUCCESS) {
    std::cerr << "Could not find proc address for vkCmdSetPrimitiveTopologyEXT."
              << std::endl;
    return;
  }
  cmdSetPolygonModeEXT(_command_buffer, VK_POLYGON_MODE_FILL);
  if (vk_result != VK_SUCCESS) {
    std::cerr << "Could not find proc address for vkCmdSetPolygonModeEXT."
              << std::endl;
    return;
  }
  cmdSetDepthWriteEnableEXT(_command_buffer, VK_TRUE);
  if (vk_result != VK_SUCCESS) {
    std::cerr << "Could not find proc address for vkCmdSetWriteEnableEXT."
              << std::endl;
    return;
  }
  VkBuffer vertex_buffers[] = {m_box_instance_buffer};
  VkDeviceSize offsets[] = {0};
  vkCmdBindVertexBuffers(_command_buffer, 0, 1, vertex_buffers, offsets);
  vkCmdDrawIndexed(_command_buffer, 36, m_boxes.size(), 0, 0, 0);

  m_boxes.clear();
  m_boxes_wire.clear();
}

void Renderer::updateBoxUniformBuffer(glm::mat4 _view_proj) {
  memcpy(m_box_uniform_buffers_mapped[m_current_frame_in_flight],
         &_view_proj, sizeof(glm::mat4));
}
