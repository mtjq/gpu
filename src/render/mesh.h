#pragma once


struct Vertex {
  using AttributesDesc =
      std::array<VkVertexInputAttributeDescription, 3>;

  alignas(16) glm::vec3 m_pos{0.0f};
  alignas(16) glm::vec3 m_colour{1.0f};
  alignas(8) glm::vec2 m_uv{0.0f};

  bool operator==(const Vertex& _rhs) const {
    return (m_pos == _rhs.m_pos &&
            m_colour == _rhs.m_colour &&
            m_uv == _rhs.m_uv);
  }

  static VkVertexInputBindingDescription getBindingDescription();
  static AttributesDesc getAttributeDescriptions();
};

namespace std {
  template<> struct hash<Vertex> {
    size_t operator()(const Vertex& _vertex) const {
      return ((hash<glm::vec3>()(_vertex.m_pos) ^
               (hash<glm::vec3>()(_vertex.m_colour) << 1)) >> 1) ^
              (hash<glm::vec2>()(_vertex.m_uv) << 1);
    }
  };
}

struct ColouredInstance {
  using AttributesDesc =
      std::array<VkVertexInputAttributeDescription, 5> ;

  alignas(16) glm::mat4 m_model_mat{1.0f};
  alignas(16) glm::vec4 m_colour{1.0f};

  static VkVertexInputBindingDescription getBindingDescription();
  static AttributesDesc getAttributeDescriptions();
};
