#pragma once


/*******************************************************************************
  Projection
*******************************************************************************/
class Projection {
public:
  Projection() = default;
  Projection(float _near, float _far);
  virtual ~Projection() = default;

  const glm::mat4& getMatrix() const;

  float getNear() const;
  void  setNear(float _near);
  float getFar() const;
  void  setFar(float _near);

  virtual void setAspectRatio(float _aspect_ratio) = 0;
  virtual void updateMatrix() = 0;

protected:
  glm::mat4 m_matrix{1.0f};

  float m_near{0.1f};
  float m_far{10'000.0f};
};


/*******************************************************************************
 PerspectiveProjection
*******************************************************************************/
class PerspectiveProjection : public Projection {
public:
  PerspectiveProjection() = default;
  PerspectiveProjection(float _fov, float _aspect_ratio,
                        float _near, float _far);
  virtual ~PerspectiveProjection() override = default;

  virtual void setAspectRatio(float _aspect_ratio) override;
  virtual void updateMatrix() override;

private:
  float m_fov{45.0f};
  float m_aspect_ratio{16.0f / 9.0f};
};


/*******************************************************************************
 OrthoProjection
*******************************************************************************/
class OrthoProjection : public Projection {
public:
  OrthoProjection() = default;
  OrthoProjection(float _right, float _top, float _near, float _far);
  virtual ~OrthoProjection() override = default;

  virtual void setAspectRatio(float _aspect_ratio) override;
  virtual void updateMatrix() override;

private:
  float m_right{20.0f};
  float m_top{20.0f};
};


/*******************************************************************************
  Camera
*******************************************************************************/
class Camera {
public:
  Camera(bool _is_perspective = true);
  Camera(const Camera&) = delete;
  Camera(Camera&&) = delete;
  Camera& operator=(const Camera&) = delete;
  Camera& operator=(Camera&&) = delete;
  ~Camera();

  const glm::mat4& getView() const;
  const glm::mat4& getProj() const;
  const glm::mat4& getViewProj() const;
  const glm::mat4& getGlobalMatrix() const;
  const glm::vec3& getPosition() const;
  float            getNear() const;
  void             setNear(float _near);
  float            getFar() const;
  void             setFar(float _far);
  bool             getIsPerspective() const;

  void setYaw(float _yaw);
  void setPitch(float _pitch);
  void setPosition(glm::vec3 _position);

  void computeWorldSpaceCornerViewDirs(glm::vec4* _out) const;
  void computeFrustrumPlanes(const glm::vec4* _corner_dirs,
                             glm::vec4* _out) const;

  void setAspectRatio(float _aspect_ratio) {
    m_projection->setAspectRatio(_aspect_ratio);
  }

  void translate(glm::vec3 _input, float _delta_time);
  void rotate(glm::vec2 _cursor_movement, float _delta_time);

  void updateView();
  void updateViewProj();

private:
  Projection* m_projection{nullptr};
  bool m_is_perspective{true};
  glm::mat4 m_global_matrix{1.0f};
  glm::mat4 m_view{1.0f};
  glm::mat4 m_view_proj{1.0f};

  glm::vec3 m_position{glm::vec3{0.0f, -120.0f, 1.85f}};
  glm::vec3 m_front{glm::vec3{0.0f, 1.0f, 0.0f}};
  glm::vec3 m_up{glm::vec3{0.0f, 0.0f, 1.0f}};
  glm::vec3 m_right{glm::vec3{1.0f, 0.0f, 0.0f}};
  glm::vec3 m_up_world{0.0f, 0.0f, 1.0f};

  float m_yaw{0.0f};
  float m_pitch{0.0f};

  float m_translation_speed{20.0f};
  float m_mouse_sensitivity{35.0f};
};
