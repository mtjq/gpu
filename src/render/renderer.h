#pragma once

#include "render/mesh.h"


const unsigned int c_frames_in_flight = 2;
const unsigned int c_max_boxes = 1'000;


class Renderer {
public:
  Renderer(const Renderer&) = delete;
  Renderer& operator=(const Renderer&) = delete;

  static Renderer& getInstance() {
    static Renderer m_instance;
    return m_instance;
  }

  static void init(GLFWwindow* _window) { getInstance().initInternal(_window); }
  static void cleanup() { getInstance().cleanupInternal(); }

  static VkCommandBuffer& beginFrame(const glm::mat4& _view_proj) {
    return getInstance().beginFrameInternal(_view_proj);
  }
  static void beginRenderPass(VkCommandBuffer& _command_buffer) {
    getInstance().beginRenderPassInternal(_command_buffer);
  }
  static void beginImGui();

  static void endImGui(VkCommandBuffer& _command_buffer);
  static void endRenderPass(VkCommandBuffer& _command_buffer) {
    vkCmdEndRenderPass(_command_buffer);
  }
  static void endFrame(VkCommandBuffer& _command_buffer);

  static VkExtent2D getSwapchainExtent() {
    return getInstance().m_swapchain_extent;
  };
  static const VkRenderPass& getRenderPass() {
    return getInstance().m_render_pass;
  }
  static unsigned int getCurrentFrameInFlight() {
    return getInstance().m_current_frame_in_flight;
  }

  static VkCommandBuffer beginSingleTimeCommands();
  static void endSingleTimeCommands(VkCommandBuffer _command_buffer);
  static VkShaderModule createShaderModule(const std::vector<char>& _code);
  static void createBuffer(VkDeviceSize _size, VkBufferUsageFlags _usage,
                           VkMemoryPropertyFlags _properties,
                           VkBuffer& _buffer, VkDeviceMemory& _bufferMemory);
  static void copyBuffer(VkBuffer _srcBuffer, VkBuffer _dstBuffer,
                         VkDeviceSize _size);

  static void initBoxRendering();
  static void addBox(const ColouredInstance& _box, bool _wired = false);
  static void renderBoxes(VkCommandBuffer& _command_buffer);

  static void setFramebufferSizeDirty(bool _val) {
    getInstance().m_framebuffer_size_dirty = _val;
  }

private:
  GLFWwindow* m_window{nullptr};

  bool m_framebuffer_size_dirty{false};

  unsigned int m_current_frame_in_flight{0};
  unsigned int m_current_swapchain_image_idx{0};

  glm::mat4 m_view_proj{1.0f};

  VkQueue m_main_queue{VK_NULL_HANDLE};
  VkQueue m_present_queue{VK_NULL_HANDLE};

  VkCommandPool                          m_main_command_pool{VK_NULL_HANDLE};
  std::array<VkCommandBuffer, c_frames_in_flight>
                                         m_main_command_buffers{VK_NULL_HANDLE};

  VkSwapchainKHR           m_swapchain{VK_NULL_HANDLE};
  std::vector<VkImage>     m_swapchain_images{VK_NULL_HANDLE};
  std::vector<VkImageView> m_swapchain_image_views{VK_NULL_HANDLE};
  VkFormat                 m_swapchain_image_format{};
  VkExtent2D               m_swapchain_extent{};

  VkRenderPass m_render_pass{VK_NULL_HANDLE};

  std::vector<VkFramebuffer> m_framebuffers{};

  VkImage        m_colour_image{VK_NULL_HANDLE};
  VkImageView    m_colour_image_view{VK_NULL_HANDLE};
  VkDeviceMemory m_colour_image_memory{VK_NULL_HANDLE};

  VkImage        m_depth_image{VK_NULL_HANDLE};
  VkImageView    m_depth_image_view{VK_NULL_HANDLE};
  VkDeviceMemory m_depth_image_memory{VK_NULL_HANDLE};

  std::array<VkSemaphore, c_frames_in_flight>
                               m_swapchain_available_semaphores{VK_NULL_HANDLE};
  std::array<VkSemaphore, c_frames_in_flight>
                               m_render_finished_semaphores{VK_NULL_HANDLE};
  std::array<VkFence, c_frames_in_flight>
                               m_in_flight_fences{VK_NULL_HANDLE};

  Renderer() {}

  void initInternal(GLFWwindow* _window);
  void cleanupInternal();
  VkCommandBuffer& beginFrameInternal(const glm::mat4& _view_proj);
  void beginRenderPassInternal(VkCommandBuffer& _command_buffer);
  void endFrameInternal(VkCommandBuffer& _command_buffer);

  void createQueues();
  void createCommandPool();
  void createCommandBuffers();
  void createSwapchain();
  void createSwapchainImages();
  void createSwapchainImageViews();
  void createRenderPass();
  void createSyncObjects();
  void createFramebuffers();
  void createColourResources();
  void createDepthResources();

  void createImage(int _width, int _height, VkFormat _format,
                   VkImageTiling _tiling, VkImageUsageFlags _usage,
                   VkMemoryPropertyFlags _properties, VkImage& _image,
                   VkDeviceMemory& _image_memory, unsigned int _mip_levels,
                   VkSampleCountFlagBits _sample_count = VK_SAMPLE_COUNT_1_BIT);
  VkImageView createImageView(VkImage _image, VkFormat _format,
                              VkImageAspectFlags _aspect_flags,
                              unsigned int _mip_levels = 1);

  void cleanupSwapchain();
  void recreateSwapchain();

  // ImGui.
  VkDescriptorPool m_imgui_descriptor_pool{VK_NULL_HANDLE};

  void initImGui(GLFWwindow* _window);
  void cleanupImGui();

  // Boxes.
  VkPipeline                        m_box_pipeline{VK_NULL_HANDLE};
  VkDescriptorPool                  m_box_descriptor_pool{VK_NULL_HANDLE};
  VkDescriptorSetLayout             m_box_descriptor_set_layout{VK_NULL_HANDLE};
  std::array<VkDescriptorSet, c_frames_in_flight>
                                    m_box_descriptor_sets{VK_NULL_HANDLE};
  VkPipelineLayout                  m_box_pipeline_layout{VK_NULL_HANDLE};

  std::vector<ColouredInstance> m_boxes{};
  std::vector<ColouredInstance> m_boxes_wire{};
  VkBuffer                  m_box_instance_buffer{VK_NULL_HANDLE};
  VkDeviceMemory            m_box_instance_buffer_memory{VK_NULL_HANDLE};
  void*                     m_box_instance_buffer_mapped{nullptr};
  VkBuffer                  m_box_index_buffer{VK_NULL_HANDLE};
  VkDeviceMemory            m_box_index_buffer_memory{VK_NULL_HANDLE};
  VkBuffer                  m_box_wire_instance_buffer{VK_NULL_HANDLE};
  VkDeviceMemory       m_box_wire_instance_buffer_memory{VK_NULL_HANDLE};
  void*                     m_box_wire_instance_buffer_mapped{nullptr};
  std::array<VkBuffer, c_frames_in_flight>
                            m_box_uniform_buffers{VK_NULL_HANDLE};
  std::array<VkDeviceMemory, c_frames_in_flight>
                            m_box_uniform_buffers_memories{VK_NULL_HANDLE};
  std::array<void*, c_frames_in_flight>
                            m_box_uniform_buffers_mapped{nullptr};

  void initBoxRenderingInternal();
  void createBoxPipeline();
  void createBoxDescriptorPool();
  void createBoxDescriptorSetLayout();
  void createBoxDescriptorSets();
  void createBoxVertexBuffers();
  void createBoxIndexBuffers();
  void createBoxUniformBuffers();

  void recordBoxCommandBuffer(VkCommandBuffer _commandBuffer,
                              unsigned int _current_frame);
  void updateBoxUniformBuffer(glm::mat4 _view_proj);
};
