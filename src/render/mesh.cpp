#include "mesh.h"


VkVertexInputBindingDescription Vertex::getBindingDescription() {
  VkVertexInputBindingDescription binding_description{};
  binding_description.binding = 0;
  binding_description.stride = sizeof(Vertex);
  binding_description.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

  return binding_description;
}

Vertex::AttributesDesc Vertex::getAttributeDescriptions() {
  AttributesDesc attribute_descriptions{};

  attribute_descriptions[0].binding = 0;
  attribute_descriptions[0].location = 0;
  attribute_descriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
  attribute_descriptions[0].offset = offsetof(Vertex, m_pos);

  attribute_descriptions[1].binding = 0;
  attribute_descriptions[1].location = 1;
  attribute_descriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
  attribute_descriptions[1].offset = offsetof(Vertex, m_colour);

  attribute_descriptions[2].binding = 0;
  attribute_descriptions[2].location = 2;
  attribute_descriptions[2].format = VK_FORMAT_R32G32_SFLOAT;
  attribute_descriptions[2].offset = offsetof(Vertex, m_uv);

  return attribute_descriptions;
}

VkVertexInputBindingDescription ColouredInstance::getBindingDescription() {
  VkVertexInputBindingDescription binding_description{};
  binding_description.binding = 0;
  binding_description.stride = sizeof(ColouredInstance);
  binding_description.inputRate = VK_VERTEX_INPUT_RATE_INSTANCE;

  return binding_description;
}

ColouredInstance::AttributesDesc ColouredInstance::getAttributeDescriptions() {
  AttributesDesc attribute_descriptions{};

  attribute_descriptions[0].binding = 0;
  attribute_descriptions[0].location = 0;
  attribute_descriptions[0].format = VK_FORMAT_R32G32B32A32_SFLOAT;
  attribute_descriptions[0].offset = offsetof(ColouredInstance, m_model_mat);

  attribute_descriptions[1].binding = 0;
  attribute_descriptions[1].location = 1;
  attribute_descriptions[1].format = VK_FORMAT_R32G32B32A32_SFLOAT;
  attribute_descriptions[1].offset = offsetof(ColouredInstance, m_model_mat) +
                                     sizeof(glm::vec4);

  attribute_descriptions[2].binding = 0;
  attribute_descriptions[2].location = 2;
  attribute_descriptions[2].format = VK_FORMAT_R32G32B32A32_SFLOAT;
  attribute_descriptions[2].offset = offsetof(ColouredInstance, m_model_mat) +
                                     2 * sizeof(glm::vec4);

  attribute_descriptions[3].binding = 0;
  attribute_descriptions[3].location = 3;
  attribute_descriptions[3].format = VK_FORMAT_R32G32B32A32_SFLOAT;
  attribute_descriptions[3].offset = offsetof(ColouredInstance, m_model_mat) +
                                     3 * sizeof(glm::vec4);

  attribute_descriptions[4].binding = 0;
  attribute_descriptions[4].location = 4;
  attribute_descriptions[4].format = VK_FORMAT_R32G32B32A32_SFLOAT;
  attribute_descriptions[4].offset = offsetof(ColouredInstance, m_colour);

  return attribute_descriptions;
}
