#include "camera.h"
#include "core/math.h"


/*******************************************************************************
  Projection
*******************************************************************************/
Projection::Projection(float _near, float _far) : m_near{_near}, m_far{_far} {
}

const glm::mat4& Projection::getMatrix() const {
  return m_matrix;
}

float Projection::getNear() const {
  return m_near;
}

void Projection::setNear(float _near) {
  m_near = _near;
  updateMatrix();
}

float Projection::getFar() const {
  return m_far;
}

void Projection::setFar(float _far) {
  m_far = _far;
  updateMatrix();
}


/*******************************************************************************
  PerspectiveProjection
*******************************************************************************/
PerspectiveProjection::PerspectiveProjection(float _fov, float _aspect_ratio,
                                             float _near, float _far)
    : Projection(_near, _far)
    , m_fov{_fov}
    , m_aspect_ratio{_aspect_ratio} {
  updateMatrix();
}

void PerspectiveProjection::setAspectRatio(float _aspect_ratio) {
  m_aspect_ratio = _aspect_ratio;
  updateMatrix();
}

void PerspectiveProjection::updateMatrix() {
  m_matrix = glm::perspective(glm::radians(m_fov),
                              m_aspect_ratio,
                              m_near, m_far);
  m_matrix[1][1] *= -1.0f; // Y down in screen space in Vulkan.
  // Depth from 1 to 0 (instead of 0 to 1).
  // Normally, because view to screen space is in -1/z, we get a huge part
  // of the frustum around a depth value of 1,
  // but floats are more precise around 0.
  m_matrix[2][2] = m_near / (m_far - m_near);
  m_matrix[3][2] = m_near * m_far / (m_far - m_near);
}

/*******************************************************************************
  OrthoProjection
*******************************************************************************/
OrthoProjection::OrthoProjection(float _right, float _top,
                                 float _near, float _far)
    : Projection(_near, _far)
    , m_right{_right}
    , m_top{_top} {
  updateMatrix();
}

void OrthoProjection::setAspectRatio(float _aspect_ratio) {
  m_right = _aspect_ratio * m_top;
  updateMatrix();
}

void OrthoProjection::updateMatrix() {
  m_matrix = glm::ortho(-m_right, m_right, -m_top, m_top, m_near, m_far);
  m_matrix[1][1] *= -1.0f; // Y down in screen space in Vulkan.
  // Depth from 1 to 0 (instead of 0 to 1).
  // Normally, because view to screen space is in -1/z, we get a huge part
  // of the frustum around a depth value of 1,
  // but floats are more precise around 0.
  m_matrix[2][2] = 1.0f / (m_far - m_near);
  m_matrix[3][2] = m_far / (m_far - m_near);
}

/*******************************************************************************
  Camera
*******************************************************************************/
Camera::Camera(bool _is_perspective) : m_is_perspective(_is_perspective) {
  if (_is_perspective) {
    m_projection = new PerspectiveProjection();
  } else {
    m_projection = new OrthoProjection();
  }
  updateView();
}

Camera::~Camera() {
  delete m_projection;
}

const glm::mat4& Camera::getView() const {
  return m_view;
}

const glm::mat4& Camera::getProj() const {
  return m_projection->getMatrix();
}

const glm::mat4& Camera::getViewProj() const {
  return m_view_proj;
}

const glm::mat4& Camera::getGlobalMatrix() const {
  return m_global_matrix;
}

const glm::vec3& Camera::getPosition() const {
  return m_position;
}

float Camera::getNear() const {
  return m_projection->getNear();
}

void Camera::setNear(float _near) {
  m_projection->setNear(_near);
  updateViewProj();
}

float Camera::getFar() const {
  return m_projection->getFar();
}

void Camera::setFar(float _far) {
  m_projection->setFar(_far);
  updateViewProj();
}

bool Camera::getIsPerspective() const {
  return m_is_perspective;
}

void Camera::setYaw(float _yaw) {
  m_yaw = _yaw;
  updateView();
  updateViewProj();
}

void Camera::setPitch(float _pitch) {
  m_pitch = _pitch;
  updateView();
  updateViewProj();
}

void Camera::setPosition(glm::vec3 _position) {
  m_position = _position;
  m_global_matrix[3] = glm::vec4(_position, 0.0f);
  updateViewProj();
}

void Camera::computeWorldSpaceCornerViewDirs(glm::vec4* _out) const {
  glm::vec4 TL{-1.0, -1.0, 1.0, 1.0};
  glm::vec4 TR{ 1.0, -1.0, 1.0, 1.0};
  glm::vec4 BL{-1.0,  1.0, 1.0, 1.0};
  glm::vec4 BR{ 1.0,  1.0, 1.0, 1.0};
  glm::mat4 invProj = glm::inverse(m_projection->getMatrix());
  TL = invProj * TL;
  TR = invProj * TR;
  BL = invProj * BL;
  BR = invProj * BR;
  TL /= TL.w;
  TR /= TR.w;
  BL /= BL.w;
  BR /= BR.w;
  glm::mat3 upper3x3 = getUpper3x3(m_global_matrix);
  _out[0] = glm::vec4(upper3x3 * glm::vec3(TL), 0.0f);
  _out[1] = glm::vec4(upper3x3 * glm::vec3(TR), 0.0f);
  _out[2] = glm::vec4(upper3x3 * glm::vec3(BL), 0.0f);
  _out[3] = glm::vec4(upper3x3 * glm::vec3(BR), 0.0f);
}

void Camera::computeFrustrumPlanes(const glm::vec4* _corner_dirs,
                                   glm::vec4* _out) const {
  // Frustum corner positions.
  glm::vec3 ntl = m_position + _corner_dirs[0].xyz();
  glm::vec3 ntr = m_position + _corner_dirs[1].xyz();
  glm::vec3 nbl = m_position + _corner_dirs[2].xyz();
  glm::vec3 nbr = m_position + _corner_dirs[3].xyz();
  glm::vec3 ftl = m_position + m_projection->getFar() * _corner_dirs[0].xyz();
  glm::vec3 ftr = m_position + m_projection->getFar() * _corner_dirs[1].xyz();
  glm::vec3 fbl = m_position + m_projection->getFar() * _corner_dirs[2].xyz();
  glm::vec3 fbr = m_position + m_projection->getFar() * _corner_dirs[3].xyz();

  // Frustum planes normals.
  glm::vec3 nn = glm::normalize(glm::cross(ntr - ntl, nbl - ntl));
  glm::vec3 fn = -nn;
  glm::vec3 tn = normalize(glm::cross(ftr - ftl, ntl - ftl));
  glm::vec3 bn = normalize(glm::cross(nbl - fbl, fbr - fbl));
  glm::vec3 ln = normalize(glm::cross(nbl - ntl, ftl - ntl));
  glm::vec3 rn = normalize(glm::cross(ftr - ntr, nbr - ntr));

  // Frumtum planes distance to origin.
  float nd = glm::dot(ntl, nn);
  float fd = glm::dot(ftl, fn);
  float td = glm::dot(ntl, tn);
  float bd = glm::dot(nbl, bn);
  float ld = glm::dot(ntl, ln);
  float rd = glm::dot(ntr, rn);

  _out[0] = glm::vec4(nn, nd);
  _out[1] = glm::vec4(fn, fd);
  _out[2] = glm::vec4(tn, td);
  _out[3] = glm::vec4(bn, bd);
  _out[4] = glm::vec4(ln, ld);
  _out[5] = glm::vec4(rn, rd);
}

void Camera::translate(glm::vec3 _input, float _delta_time) {
  m_position += m_translation_speed * _delta_time * _input.x * m_right;
  m_position += m_translation_speed * _delta_time * _input.y * m_front;
  m_position += m_translation_speed * _delta_time * _input.z * m_up;

  updateView();
}

void Camera::rotate(glm::vec2 _cursor_movement, float _delta_time) {
  m_yaw -= m_mouse_sensitivity * _delta_time * _cursor_movement.x;
  m_pitch -= m_mouse_sensitivity * _delta_time * _cursor_movement.y;

  m_pitch = std::clamp(m_pitch, -89.0f, 89.0f);

  updateView();
}

void Camera::updateView() {
  glm::vec3 front;
  front.x = - cos(glm::radians(m_pitch)) * sin(glm::radians(m_yaw));
  front.y = cos(glm::radians(m_pitch)) * cos(glm::radians(m_yaw));
  front.z = sin(glm::radians(m_pitch));
  m_front = glm::normalize(front);

  m_right = glm::normalize(glm::cross(m_front, m_up_world));
  m_up = glm::normalize(glm::cross(m_right, m_front));

  m_view = glm::lookAt(m_position, m_position + m_front, m_up);
  m_global_matrix = glm::affineInverse(m_view);

  updateViewProj();
}

void Camera::updateViewProj() {
  m_view_proj = m_projection->getMatrix() * m_view;
}
