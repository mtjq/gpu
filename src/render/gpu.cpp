#include "gpu.h"


const std::array<const char*, 1> validation_layers = {
  "VK_LAYER_KHRONOS_validation"
};

const std::array<const char*, 1> instance_extensions = {
  VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME
};

const std::array<const char*, 4> device_extensions = {
  VK_KHR_SWAPCHAIN_EXTENSION_NAME,
  VK_EXT_EXTENDED_DYNAMIC_STATE_EXTENSION_NAME,
  VK_EXT_EXTENDED_DYNAMIC_STATE_2_EXTENSION_NAME,
  VK_EXT_EXTENDED_DYNAMIC_STATE_3_EXTENSION_NAME,
};


void Gpu::init(GLFWwindow* _window) {
  Gpu& gpu = getInstance();
  gpu.createInstance();
  // TODO: setup debug messenger.
  gpu.createSurface(_window);
  gpu.pickPhysicalDevice();
  gpu.createLogicalDevice();
}

void Gpu::cleanup() {
  vkDestroyDevice(getDevice(), nullptr); // Automatically cleans up queues.
  vkDestroySurfaceKHR(getVkInstance(), getSurface(), nullptr);
  vkDestroyInstance(getVkInstance(), nullptr);
}

uint32_t Gpu::getSwapchainImageCount() {
  SwapchainSupportDetails swapchain_support = getSwapchainSupport();
  uint32_t image_count = swapchain_support.m_capabilities.minImageCount + 1;
  if (swapchain_support.m_capabilities.maxImageCount > 0 &&
      image_count > swapchain_support.m_capabilities.maxImageCount) {
    image_count = swapchain_support.m_capabilities.maxImageCount;
  }
  return image_count;
}

VkExtent2D Gpu::chooseSwapExtent(
    GLFWwindow* _window, const VkSurfaceCapabilitiesKHR& _capabilities) {
  if (_capabilities.currentExtent.width !=
      std::numeric_limits<uint32_t>::max()) {
    return _capabilities.currentExtent;
  } else {
    int width, height;
    // k_width and k_height are in screen coordinates, but it can differ
    // from the pixel coordinates asked by Vulkan here.
    glfwGetFramebufferSize(_window, &width, &height);

    VkExtent2D actual_extent = {static_cast<uint32_t>(width),
                                static_cast<uint32_t>(height)};

    actual_extent.width = std::clamp(actual_extent.width,
                                     _capabilities.minImageExtent.width,
                                     _capabilities.maxImageExtent.width);
    actual_extent.height = std::clamp(actual_extent.height,
                                      _capabilities.minImageExtent.height,
                                      _capabilities.maxImageExtent.height);

    return actual_extent;
  }
}

uint32_t Gpu::findMemoryType(uint32_t _type_filter,
                             VkMemoryPropertyFlags _properties) {
  VkPhysicalDeviceMemoryProperties mem_properties;
  vkGetPhysicalDeviceMemoryProperties(getPhysicalDevice(), &mem_properties);

  for (uint32_t i = 0; i < mem_properties.memoryTypeCount; ++i) {
    VkMemoryPropertyFlags current_properties =
        mem_properties.memoryTypes[i].propertyFlags & _properties;
    if ((_type_filter & (1 << i)) && current_properties == _properties) {
      return i;
    }
  }

  throw std::runtime_error("failed to find suitable memory type");
}

VkFormat Gpu::findDepthFormat() {
  return getInstance().findSupportedFormat(
      {VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT,
          VK_FORMAT_D24_UNORM_S8_UINT},
      VK_IMAGE_TILING_OPTIMAL,
      VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);
}

static bool checkValidationLayerSupport() {
  uint32_t layer_count;
  vkEnumerateInstanceLayerProperties(&layer_count, nullptr);

  std::vector<VkLayerProperties> available_layers(layer_count);
  vkEnumerateInstanceLayerProperties(&layer_count, available_layers.data());

  for (const char* layer_name : validation_layers) {
    bool layer_found = false;

    for (const VkLayerProperties& layer_properties : available_layers) {
      if (strcmp(layer_name, layer_properties.layerName) == 0) {
        layer_found = true;
        break;
      }
    }

    if (!layer_found) {
      return false;
    }
  }

  return true;
}

void Gpu::createInstance() {
  if (enable_validation_layers && !checkValidationLayerSupport()) {
    throw std::runtime_error("validation layers requested, but not available");
  }

  VkApplicationInfo app_info{};
  app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
  app_info.pApplicationName = "Boids";
  app_info.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
  app_info.pEngineName = "No engine";
  app_info.engineVersion = VK_MAKE_VERSION(1, 0, 0);
  app_info.apiVersion = VK_API_VERSION_1_0;

  VkInstanceCreateInfo create_info{};
  create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
  create_info.pApplicationInfo = &app_info;

  if (enable_validation_layers) {
    create_info.enabledLayerCount = validation_layers.size();
    create_info.ppEnabledLayerNames = validation_layers.data();
  } else {
    create_info.enabledLayerCount = 0;
  }

  uint32_t glfw_extension_count = 0;
  const char** glfwExtensions;

  glfwExtensions = glfwGetRequiredInstanceExtensions(&glfw_extension_count);

  std::vector<const char*> extensions(instance_extensions.size() +
                                glfw_extension_count);
  int idx = 0;
  for (const char* extension : instance_extensions) {
    extensions[idx++] = extension;
  }
  for (unsigned int i = 0; i < glfw_extension_count; ++i) {
    extensions[idx + i] = glfwExtensions[i];
  }

  create_info.enabledExtensionCount = extensions.size();
  create_info.ppEnabledExtensionNames = extensions.data();

  if(vkCreateInstance(&create_info, nullptr, &m_instance) != VK_SUCCESS) {
    throw std::runtime_error("failed to create instance");
  }
}

void Gpu::createSurface(GLFWwindow* _window) {
  VkResult create_result = glfwCreateWindowSurface(m_instance, _window,
                                                   nullptr, &m_surface);
  if ( create_result != VK_SUCCESS) {
    throw std::runtime_error("failed to create window surface");
  }
}

void Gpu::pickPhysicalDevice() {
  uint32_t device_count = 0;
  vkEnumeratePhysicalDevices(m_instance, &device_count, nullptr);

  if (device_count == 0) {
    throw std::runtime_error("failed to find GPU with Vulkan support");
  }

  std::vector<VkPhysicalDevice> devices(device_count);
  vkEnumeratePhysicalDevices(m_instance, &device_count, devices.data());

  for (const VkPhysicalDevice& device : devices) {
    if (isDeviceSuitable(device)) {
      m_physical_device = device;
      m_msaa_samples = getMaxUsableSampleCount();
      break;
    }
  }

  if (m_physical_device == VK_NULL_HANDLE) {
    throw std::runtime_error("failed to find a suitable GPU");
  }
}

void Gpu::createLogicalDevice() {
  QueueFamilyIndices indices = queryQueueFamilies(m_physical_device);

  std::vector<VkDeviceQueueCreateInfo> queue_create_infos;
  std::set<uint32_t> unique_queue_families = {
    indices.m_main_family.value(),
    indices.m_present_family.value()
  };

  float queue_priority = 1.0f;
  for (uint32_t queue_family : unique_queue_families) {
    VkDeviceQueueCreateInfo queue_create_info{};
    queue_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queue_create_info.queueFamilyIndex = queue_family;
    queue_create_info.queueCount = 1;
    queue_create_info.pQueuePriorities = &queue_priority;
    queue_create_infos.push_back(queue_create_info);
  }

  VkPhysicalDeviceFeatures device_features{};
  device_features.samplerAnisotropy = VK_TRUE;
  device_features.sampleRateShading = VK_TRUE;
  device_features.fillModeNonSolid = VK_TRUE;

  VkPhysicalDeviceExtendedDynamicState3FeaturesEXT ext_dyn_state3_features{};
  ext_dyn_state3_features.sType =
      VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTENDED_DYNAMIC_STATE_3_FEATURES_EXT;
  ext_dyn_state3_features.extendedDynamicState3PolygonMode = VK_TRUE;

  VkPhysicalDeviceExtendedDynamicStateFeaturesEXT ext_dyn_state_feature{};
  ext_dyn_state_feature.sType =
      VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTENDED_DYNAMIC_STATE_FEATURES_EXT;
  ext_dyn_state_feature.extendedDynamicState = VK_TRUE;
  ext_dyn_state_feature.pNext = &ext_dyn_state3_features;

  VkPhysicalDeviceFeatures2 device_features2{};
  device_features2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
  device_features2.features = device_features;
  device_features2.pNext = &ext_dyn_state_feature;

  VkDeviceCreateInfo create_info{};
  create_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
  create_info.queueCreateInfoCount = queue_create_infos.size();
  create_info.pQueueCreateInfos = queue_create_infos.data();
  create_info.pEnabledFeatures = VK_NULL_HANDLE;
  create_info.pNext = &device_features2;

  create_info.enabledExtensionCount = device_extensions.size();
  create_info.ppEnabledExtensionNames = device_extensions.data();

  // Ignored with up-to-date API, but good to have for compatibilty;
  if (enable_validation_layers) {
    create_info.enabledLayerCount = validation_layers.size();
    create_info.ppEnabledLayerNames = validation_layers.data();
  } else {
    create_info.enabledLayerCount = 0;
  }

  VkResult create_result = vkCreateDevice(m_physical_device, &create_info,
                                          nullptr, &m_device);
  if (create_result != VK_SUCCESS) {
    throw std::runtime_error("failed to create logical device");
  }

}

static bool checkDeviceExtensionsSupport(VkPhysicalDevice _device) {
  uint32_t extension_count;
  vkEnumerateDeviceExtensionProperties(_device, nullptr,
                                       &extension_count, nullptr);
  std::vector<VkExtensionProperties> available_extensions(extension_count);
  vkEnumerateDeviceExtensionProperties(_device, nullptr,
                                       &extension_count,
                                       available_extensions.data());

  std::set<std::string> required_extensions(device_extensions.begin(),
                                            device_extensions.end());

  for (const VkExtensionProperties& extension : available_extensions) {
    required_extensions.erase(extension.extensionName);
  }

  return required_extensions.empty();
}

bool Gpu::isDeviceSuitable(VkPhysicalDevice _physical_device) {
  VkPhysicalDeviceProperties device_properties;
  vkGetPhysicalDeviceProperties(_physical_device, &device_properties);
  bool valid_properties = device_properties.deviceType ==
                          VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU;

  VkPhysicalDeviceFeatures device_features;
  vkGetPhysicalDeviceFeatures(_physical_device, &device_features);
  bool valid_features = device_features.geometryShader &&
                        device_features.samplerAnisotropy &&
                        device_features.fillModeNonSolid;

  QueueFamilyIndices indices = queryQueueFamilies(_physical_device);
  bool valid_queues = indices.isComplete();

  bool extensions_supported = checkDeviceExtensionsSupport(_physical_device);

  bool swapchain_adequate = false;
  if (extensions_supported) {
    SwapchainSupportDetails swapchainSupport =
        querySwapchainSupport(_physical_device);
    swapchain_adequate = !swapchainSupport.m_formats.empty() &&
                         !swapchainSupport.m_present_modes.empty();
  }

  return (valid_properties && valid_features && valid_queues &&
          extensions_supported && swapchain_adequate);
}

VkSampleCountFlagBits Gpu::getMaxUsableSampleCount() {
  VkPhysicalDeviceProperties physical_device_properties;
  vkGetPhysicalDeviceProperties(m_physical_device, &physical_device_properties);

  VkSampleCountFlags counts =
      physical_device_properties.limits.framebufferColorSampleCounts &
      physical_device_properties.limits.framebufferDepthSampleCounts;
  if (counts & VK_SAMPLE_COUNT_64_BIT) { return VK_SAMPLE_COUNT_64_BIT; }
  if (counts & VK_SAMPLE_COUNT_32_BIT) { return VK_SAMPLE_COUNT_32_BIT; }
  if (counts & VK_SAMPLE_COUNT_16_BIT) { return VK_SAMPLE_COUNT_16_BIT; }
  if (counts & VK_SAMPLE_COUNT_8_BIT) { return VK_SAMPLE_COUNT_8_BIT; }
  if (counts & VK_SAMPLE_COUNT_4_BIT) { return VK_SAMPLE_COUNT_4_BIT; }
  if (counts & VK_SAMPLE_COUNT_2_BIT) { return VK_SAMPLE_COUNT_2_BIT; }

  return VK_SAMPLE_COUNT_1_BIT;
}

Gpu::QueueFamilyIndices Gpu::queryQueueFamilies(
    VkPhysicalDevice _physical_device) {
  QueueFamilyIndices indices;

  uint32_t queue_family_count = 0;
  vkGetPhysicalDeviceQueueFamilyProperties(_physical_device,
                                           &queue_family_count, nullptr);

  std::vector<VkQueueFamilyProperties> queue_families(queue_family_count);
  vkGetPhysicalDeviceQueueFamilyProperties(_physical_device,
                                           &queue_family_count,
                                           queue_families.data());

  int i = 0;
  for (const VkQueueFamilyProperties& queue_family : queue_families) {
    if ((queue_family.queueFlags & VK_QUEUE_GRAPHICS_BIT) &&
        (queue_family.queueFlags & VK_QUEUE_COMPUTE_BIT)) {
      indices.m_main_family = i;
    }

    VkBool32 present_support = false;
    vkGetPhysicalDeviceSurfaceSupportKHR(_physical_device, i, m_surface,
                                         &present_support);
    if (present_support) {
      indices.m_present_family = i;
    }

    if (indices.isComplete()) {
      break;
    }

    ++i;
  }

  return indices;
}

Gpu::SwapchainSupportDetails Gpu::querySwapchainSupport(
    VkPhysicalDevice _physical_device) {
  SwapchainSupportDetails details;

  vkGetPhysicalDeviceSurfaceCapabilitiesKHR(_physical_device, m_surface,
                                            &details.m_capabilities);

  uint32_t format_count;
  vkGetPhysicalDeviceSurfaceFormatsKHR(_physical_device, m_surface,
                                       &format_count,
                                       nullptr);
  if (format_count != 0) {
    details.m_formats.resize(format_count);
    vkGetPhysicalDeviceSurfaceFormatsKHR(_physical_device, m_surface,
                                         &format_count,
                                         details.m_formats.data());
  }

  uint32_t present_mode_count;
  vkGetPhysicalDeviceSurfacePresentModesKHR(_physical_device, m_surface,
                                            &present_mode_count,
                                            nullptr);
  if (present_mode_count != 0) {
    details.m_present_modes.resize(present_mode_count);
    vkGetPhysicalDeviceSurfacePresentModesKHR(_physical_device, m_surface,
                                              &present_mode_count,
                                              details.m_present_modes.data());
  }

  return details;
}

VkFormat Gpu::findSupportedFormat(const std::vector<VkFormat>& _candidates,
                                  VkImageTiling _tiling,
                                  VkFormatFeatureFlags _features) {
  for (VkFormat format : _candidates) {
    VkFormatProperties props;
    vkGetPhysicalDeviceFormatProperties(m_physical_device, format, &props);
    if (_tiling == VK_IMAGE_TILING_LINEAR &&
        (props.linearTilingFeatures & _features) == _features) {
      return format;
    } else if (_tiling == VK_IMAGE_TILING_OPTIMAL &&
               (props.optimalTilingFeatures & _features) == _features) {
      return format;
    }
  }

  throw std::runtime_error("failed to find supported format");
}
