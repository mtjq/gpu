#include "utils.h"


std::vector<char> readFile(const std::filesystem::path& _filename) {
  std::ifstream file(_filename, std::ios::ate | std::ios::binary);
  if (!file.is_open()) {
    throw std::runtime_error("failed to open file " + _filename.string());
  }

  size_t file_size = file.tellg();
  std::vector<char> buffer(file_size);
  file.seekg(0);
  file.read(buffer.data(), file_size);

  file.close();
  return buffer;
}

