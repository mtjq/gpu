#include "input.h"


/*******************************************************************************
  Callbacks.
*******************************************************************************/
void keyCallback(GLFWwindow* /* _window */, int _key,
                        int /* _scancode */, int _action, int /* _mods */) {
  Input::setKeyState(_key, _action);
}

void cursorPosCallback(GLFWwindow* /* _window */,
                              double _xpos, double _ypos) {
  Input::setCursorPos(glm::vec2{_xpos, _ypos});
}

void mouseButtonCallback(GLFWwindow* /* _window */,
                                int _button, int _action, int /* _mods */) {
  Input::setMouseButtonState(_button, _action);
}


/*******************************************************************************
  Input
*******************************************************************************/
Input& Input::getInstance() {
  static Input m_instance;
  return m_instance;
}

void Input::init(GLFWwindow* _window) {
  Input& instance = getInstance();
  instance.m_window = _window;
  glfwSetKeyCallback(_window, keyCallback);
  glfwSetCursorPosCallback(_window, cursorPosCallback);
  glfwSetMouseButtonCallback(_window, mouseButtonCallback);

  double cursor_xpos, cursor_ypos;
  glfwGetCursorPos(instance.m_window, &cursor_xpos, &cursor_ypos);
  instance.m_cursor_pos = glm::vec2{cursor_xpos, cursor_ypos};
}

void Input::setKeyState(int _key, int _action) {
  Input& instance = getInstance();
  if (_action == GLFW_PRESS) {
    instance.m_key_states[_key] = InputKeyState::JustPressed;
    instance.m_keys_to_update.insert(_key);
    return;
  }
  if (_action == GLFW_RELEASE) {
    instance.m_key_states[_key] = InputKeyState::JustReleased;
    instance.m_keys_to_update.insert(_key);
    return;
  }
}

void Input::setCursorPos(glm::vec2 _pos) {
  Input& instance = getInstance();
  instance.m_cursor_movement = _pos - instance.m_cursor_pos;
  instance.m_cursor_pos = _pos;
}

void Input::setMouseButtonState(int _button, int _action) {
  Input& instance = getInstance();
  if (_action == GLFW_PRESS) {
    instance.m_mouse_button_states[_button] = InputKeyState::JustPressed;
    instance.m_mouse_buttons_to_update.insert(_button);
    return;
  }
  if (_action == GLFW_RELEASE) {
    instance.m_mouse_button_states[_button] = InputKeyState::JustReleased;
    instance.m_mouse_buttons_to_update.insert(_button);
    return;
  }
}

bool Input::isKeyPressed(int _key) {
  return (getInstance().m_key_states[_key] == InputKeyState::Pressed
          || getInstance().m_key_states[_key] == InputKeyState::JustPressed);
}

bool Input::isKeyReleased(int _key) {
  return (getInstance().m_key_states[_key] == InputKeyState::Released
          || getInstance().m_key_states[_key] == InputKeyState::JustReleased);
}

bool Input::isKeyJustPressed(int _key) {
  return getInstance().m_key_states[_key] == InputKeyState::JustPressed;
}

bool Input::isKeyJustReleased(int _key) {
  return getInstance().m_key_states[_key] == InputKeyState::JustReleased;
}

bool Input::isMouseButtonPressed(int _button) {
  return (getInstance().m_mouse_button_states[_button] ==
              InputKeyState::JustPressed
          || getInstance().m_mouse_button_states[_button] ==
              InputKeyState::Pressed);
}

bool Input::isMouseButtonReleased(int _button) {
  return (getInstance().m_mouse_button_states[_button] ==
              InputKeyState::JustReleased
          || getInstance().m_mouse_button_states[_button] ==
              InputKeyState::Released);
}

bool Input::isMouseButtonJustPressed(int _button) {
  return getInstance().m_mouse_button_states[_button] ==
      InputKeyState::JustPressed;
}

bool Input::isMouseButtonJustReleased(int _button) {
  return  getInstance().m_mouse_button_states[_button] ==
              InputKeyState::JustReleased;
}

glm::vec2 Input::getCursorMovement() {
  return getInstance().m_cursor_movement;
}

glm::vec2 Input::getScrollMovement() {
  return getInstance().m_scroll_movement;
}

void Input::endFrame() {
  Input& instance = getInstance();
  instance.m_cursor_movement = glm::vec2{0.0f};
  instance.m_scroll_movement = glm::vec2{0.0f};

  for (int key_to_update : instance.m_keys_to_update) {
    switch (instance.m_key_states[key_to_update]) {
      case InputKeyState::JustPressed:
        instance.m_key_states[key_to_update] = InputKeyState::Pressed;
        break;
      case InputKeyState::JustReleased:
        instance.m_key_states[key_to_update] = InputKeyState::Released;
        break;
      default:
        break;
    }
  }
  instance.m_keys_to_update.clear();

  for (int mouse_button_to_update : instance.m_mouse_buttons_to_update) {
    switch (instance.m_mouse_button_states[mouse_button_to_update]) {
      case InputKeyState::JustPressed:
        instance.m_mouse_button_states[mouse_button_to_update] =
            InputKeyState::Pressed;
        break;
      case InputKeyState::JustReleased:
        instance.m_mouse_button_states[mouse_button_to_update] =
            InputKeyState::Released;
        break;
      default:
        break;
    }
  }
  instance.m_mouse_buttons_to_update.clear();
}
