#include "math.h"


glm::mat3 getUpper3x3(const glm::mat4& _in) {
  return glm::mat3(glm::vec3(_in[0]), glm::vec3(_in[1]), glm::vec3(_in[2]));
}
