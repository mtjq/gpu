#pragma once


/*******************************************************************************
  FilesystemManager
*******************************************************************************/
class FilesystemManager {
public:
  FilesystemManager(const FilesystemManager&) = delete;
  FilesystemManager(FilesystemManager&&) = delete;
  FilesystemManager& operator=(const FilesystemManager&) = delete;
  FilesystemManager& operator=(FilesystemManager&&) = delete;
  ~FilesystemManager() = default;

  static void Init();
  static std::filesystem::path FromRootDirRelativeToAbsolute(
      const std::filesystem::path& _relative_path);

private:
  std::filesystem::path m_exe_parent_path;

  FilesystemManager() = default;

  static FilesystemManager& getInstance() {
    static FilesystemManager m_instance;
    return m_instance;
  }
};
