#pragma once


/*******************************************************************************
  Input
*******************************************************************************/
class Input {
public:
  Input(const Input&) = delete;
  Input(Input&&) = delete;
  Input& operator=(const Input&) = delete;
  Input& operator=(Input&&) = delete;
  ~Input() = default;

  static Input& getInstance();

  static void init(GLFWwindow* _window);

  static void setKeyState(int _key, int _action);
  static void setCursorPos(glm::vec2 _pos);
  static void setMouseButtonState(int _button, int _action);

  static bool isKeyPressed(int _key);
  static bool isKeyReleased(int _key);
  static bool isKeyJustPressed(int _key);
  static bool isKeyJustReleased(int _key);
  static bool isMouseButtonPressed(int _button);
  static bool isMouseButtonReleased(int _button);
  static bool isMouseButtonJustPressed(int _button);
  static bool isMouseButtonJustReleased(int _button);
  static glm::vec2 getCursorMovement();
  static glm::vec2 getScrollMovement();

  static void endFrame();

private:
  enum class InputKeyState {
    Released,
    JustPressed,
    Pressed,
    JustReleased
  };

  Input() = default;

  GLFWwindow* m_window{nullptr};

  glm::vec2 m_cursor_pos{glm::vec2{0.0f}};
  glm::vec2 m_cursor_movement{glm::vec2{0.0f}};
  glm::vec2 m_scroll_movement{glm::vec2{0.0f}};

  InputKeyState m_key_states[GLFW_KEY_LAST]{InputKeyState::Released};
  InputKeyState m_mouse_button_states[GLFW_MOUSE_BUTTON_LAST]{
      InputKeyState::Released};
  std::set<int> m_keys_to_update{};
  std::set<int> m_mouse_buttons_to_update{};
};
