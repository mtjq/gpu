#pragma once


constexpr float c_PI = 3.14159265358979323846;


glm::mat3 getUpper3x3(const glm::mat4& _in);
