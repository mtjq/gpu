#include "filesystem_manager.h"

#if defined(_WIN32)
#include <windows.h>
#include <libloaderapi.h>
#endif


/*******************************************************************************
  FilesystemManager
*******************************************************************************/
void FilesystemManager::Init() {
  FilesystemManager& instance = getInstance();
#if defined(__unix__)
  std::filesystem::path exe_path = std::filesystem::canonical("/proc/self/exe");
  instance.m_exe_parent_path = exe_path.parent_path();
#elif defined(_WIN32)
  const DWORD buffer_size = 512;
  CHAR buffer[buffer_size];
  DWORD path_length = GetModuleFileName(nullptr, buffer, buffer_size);
  if (path_length == 0 || path_length == buffer_size) {
    throw std::runtime_error("exe path length exceeds"
                             "the maximum supported length");
  }
  instance.m_exe_parent_path = std::filesystem::path(buffer).parent_path();
#else
  throw std::runtime_error("platform not supported");
#endif
}

std::filesystem::path FilesystemManager::FromRootDirRelativeToAbsolute(
      const std::filesystem::path& _relative_path) {
  return getInstance().m_exe_parent_path / "../../" / _relative_path;
}
