#include "app/app.h"
#include "core/filesystem_manager.h"


int main() {
  FilesystemManager::Init();
  App app{};

  try {
    app.run();
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
