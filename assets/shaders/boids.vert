#version 450

layout(binding = 0) uniform UniformBufferObject {
  mat4 m_view_proj;
  float m_size;
} ubo;

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_velocity;

layout(location = 0) out vec3 frag_colour;


const float c_pi = 3.1415926535897932384626433832795;

const vec3 vertices[4] = vec3[](
  0.4f * vec3(1.0f, 0.0f, 0.0f),
  0.4f * vec3(cos(2.0f / 3.0f * c_pi),  sin(2.0f / 3.0f * c_pi), 0.0f),
  0.4f * vec3(cos(2.0f / 3.0f * c_pi), -sin(2.0f / 3.0f * c_pi), 0.0f),
  vec3(0.0f, 0.0f, 1.0f)
);

const int indices[12] = int[](
  0, 2, 1,
  0, 1, 3,
  1, 2, 3,
  2, 0, 3
);

void main() {
  vec3 boid_dir = normalize(in_velocity);
  float cos_theta = boid_dir.z;
  vec3 rot_vec_sin_theta = cross(vec3(0.0f, 0.0f, 1.0f), boid_dir);
  vec3 rot_vec = normalize(rot_vec_sin_theta);

  vec3 vertex_pos = ubo.m_size * vertices[indices[gl_VertexIndex]];
  vertex_pos = cos_theta * vertex_pos +
               cross(rot_vec_sin_theta, vertex_pos) +
               (1.0f - cos_theta) * dot(rot_vec, vertex_pos) * rot_vec;

  vec4 vertex_pos_world = vec4(vertex_pos + in_position, 1.0f);
  gl_Position = ubo.m_view_proj * vertex_pos_world;
  frag_colour = 0.7f * normalize(abs(in_velocity));
}

