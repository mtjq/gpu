#version 450

layout(binding = 0) uniform UniformBufferObject {
  mat4 m_view_proj;
} ubo;

layout(location = 0) in mat4 in_model_mat;
layout(location = 4) in vec4 in_colour;

layout(location = 0) out vec4 frag_colour;

vec4 vertices[8] = vec4[](
    vec4(-0.5f, -0.5f,  0.5f, 1.0f),
    vec4( 0.5f, -0.5f,  0.5f, 1.0f),
    vec4(-0.5f,  0.5f,  0.5f, 1.0f),
    vec4( 0.5f,  0.5f,  0.5f, 1.0f),
    vec4(-0.5f, -0.5f, -0.5f, 1.0f),
    vec4( 0.5f, -0.5f, -0.5f, 1.0f),
    vec4(-0.5f,  0.5f, -0.5f, 1.0f),
    vec4( 0.5f,  0.5f, -0.5f, 1.0f)
);

void main() {
  mat4 mvp = ubo.m_view_proj * in_model_mat;
  gl_Position = mvp * vertices[gl_VertexIndex];
  frag_colour = in_colour;
}
