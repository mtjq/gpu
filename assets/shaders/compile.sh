glslc box.vert -o box_vert.spv
glslc box.frag -o box_frag.spv

glslc boids.vert -o boids_vert.spv
glslc boids.frag -o boids_frag.spv
glslc boids.comp -o boids_comp.spv
