# Description

This is a GPU-driven boids simulation (which can represents flocks of birds,
schools of fish, etc). It uses the three basic rules in the model:  
- separation (particles do not want to run into each other)
- alignment (they want to follow the same general direction as their closest
    neighbours)
- cohesion (they tend to go towards the center of mass of their neighbours).

![Boids](resources/boids.jpg)


# Building the project

## Dependencies

You will need Cmake and a C++ compiler.  
You will also need the Vulkan SDK to build the project. You can download it
from [the LunarG website](https://vulkan.lunarg.com/).

## Build and run

After cloning the project, simply run
`cmake -B build && cd build` and compile with your default
method (for instance `make all` on GNU/Linux or opening the Visual Studio
solution on Windows).  
The binary should be in the `bin/` folder from the root of the project.

Please note that this has not been tested on MacOS.

## Usage

You can move the camera with Q, W, E, A, S, D, and rotate it by maintaining
your right click and moving your cursor. Pressing shift/control will speed
up/down the movements.
